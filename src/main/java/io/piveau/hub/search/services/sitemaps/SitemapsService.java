package io.piveau.hub.search.services.sitemaps;

import io.piveau.hub.search.util.index.IndexManager;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface SitemapsService {

    String SERVICE_ADDRESS = "io.piveau.hub.search.services.sitemaps.queue";

    static Future<SitemapsService> create(Vertx vertx, JsonObject esConfig, IndexManager indexManager, JsonObject sitemapConfig) {
        return Future.future(promise -> new SitemapsServiceImpl(vertx, esConfig, indexManager, sitemapConfig, promise));
    }

    static SitemapsService createProxy(Vertx vertx, String address) {
        return new SitemapsServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    Future<JsonObject> readSitemapIndex();
    Future<JsonObject> readSitemap(String sitemapId);
    Future<JsonObject> triggerSitemapGeneration();
}
