package io.piveau.hub.search.services.datasets;

import co.elastic.clients.json.JsonData;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.services.catalogues.CataloguesService;
import io.piveau.hub.search.services.search.SearchService;
import io.piveau.hub.search.services.vocabulary.VocabularyService;
import io.piveau.hub.search.util.date.DateChecker;
import io.piveau.hub.search.util.geo.SpatialChecker;
import io.piveau.hub.search.util.index.HVDSetter;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.piveau.json.*;
import io.piveau.utils.PiveauContext;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class DatasetsServiceImpl implements DatasetsService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Vertx vertx;

    private final SearchClient searchClient;

    private final SearchService searchService;
    private final CataloguesService cataloguesService;
    private final VocabularyService vocabularyService;

    private HVDSetter hvdSetter;
    private final PiveauContext serviceContext;

    DatasetsServiceImpl(Vertx vertx, JsonObject config, IndexManager indexManager,
                        Handler<AsyncResult<DatasetsService>> handler) {
        this.vertx = vertx;
        JsonObject esConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_ES_CONFIG);
        this.searchClient = SearchClient.build(vertx, esConfig, indexManager);
        this.hvdSetter = new HVDSetter(indexManager, config);
        this.searchService = SearchService.createProxy(vertx, SearchService.SERVICE_ADDRESS);
        this.cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
        this.vocabularyService = VocabularyService.createProxy(vertx, VocabularyService.SERVICE_ADDRESS);

        this.serviceContext = new PiveauContext("hub.search", "DatasetsService");

        handler.handle(Future.succeededFuture(this));
    }

    private String getResourceContext(String datasetId, JsonObject payload) {
        String context = datasetId != null ? datasetId : "id-not-available";
        JsonObject catalog = payload.getJsonObject("catalog");
        if (catalog != null) {
            String catalogId = catalog.getString("id");
            if (catalogId != null && !catalogId.isEmpty()) {
                context += "; catalog: " + catalogId;
            }
        }
        return context;
    }

    @Override
    public Future<JsonArray> listDatasets(String catalogueId, String alias) {
        Promise<JsonArray> promise = Promise.promise();

        String defaultAlias = alias == null ? Constants.getReadAlias(Constants.DOC_TYPE_DATASET) : alias;

        JsonObject q = new JsonObject();
        q.put("size", 10000);
        q.put("filter", Constants.DOC_TYPE_DATASET);
        q.put("aggregation", false);
        q.put("includes", new JsonArray().add("id"));
        q.put("scroll", true);
        q.put("alias", defaultAlias);

        q.put("facets", new JsonObject().put("catalog.id.raw", new JsonArray().add(catalogueId)));

        QueryParams query = Json.decodeValue(q.toString(), QueryParams.class);

        searchClient.listIds(query, false, true).onSuccess(promise::complete).onFailure(promise::fail);

        return promise.future();
    }

    @Override
    public Future<JsonObject> createDataset(JsonObject payload) {
        Promise<JsonObject> promise = Promise.promise();

        PiveauContext resourceContext = serviceContext.extend(getResourceContext(null, payload));
        DateChecker.check(payload);
        SpatialChecker.check(payload);
        hvdSetter.set(payload);

        cataloguesService.checkCatalogueInPayloadObject(payload)
                .compose(vocabularyService::replaceVocabularyInPayload)
                .onSuccess(replaceVocabularyResult ->
                        searchClient.postDocument(Constants.DOC_TYPE_DATASET, true, replaceVocabularyResult).onSuccess(result -> {
                            resourceContext.log().debug("Post success: " + result);
                            promise.complete(ReturnHelper.returnSuccess(201, new JsonObject().put("id", result)));
                        }).onFailure(promise::fail)
                ).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<JsonObject> createOrUpdateDataset(String datasetId, JsonObject payload) {
        Promise<JsonObject> promise = Promise.promise();
        PiveauContext resourceContext = serviceContext.extend(getResourceContext(datasetId, payload));
        DateChecker.check(payload);
        SpatialChecker.check(payload);
        hvdSetter.set(payload);

        cataloguesService.checkCatalogueInPayloadObject(payload)
                .compose(vocabularyService::replaceVocabularyInPayload)
                .onSuccess(replaceVocabularyResult -> {
                    Promise<Void> getOldDatasetPromise = Promise.promise();
                    searchClient.getDocument(Constants.DOC_TYPE_DATASET,
                            Constants.getReadAlias(Constants.DOC_TYPE_DATASET), datasetId, true).onSuccess(result -> {
                        if (replaceVocabularyResult.getJsonObject("quality_meas") == null ||
                                replaceVocabularyResult.getJsonObject("quality_meas").isEmpty()) {
                            replaceVocabularyResult.put("quality_meas", result.getJsonObject("quality_meas"));
                        }
                        createDatasetRevisionDEPRECATED(datasetId, result).onComplete(a -> {
                            if (!a.succeeded()) {
                                resourceContext.log().error("Write dataset revision: {}", a.cause().getMessage());
                            }
                            getOldDatasetPromise.complete();
                        });
                    }).onFailure(failure -> {
                        if (failure.getMessage().equals("not found")) {
                            getOldDatasetPromise.complete();
                        } else {
                            getOldDatasetPromise.fail(failure);
                        }
                    });
                    getOldDatasetPromise.future().onComplete(getOldDatasetResult -> {
                        searchClient.putDocument(Constants.DOC_TYPE_DATASET, datasetId, true, replaceVocabularyResult)
                                .onSuccess(result -> {
                                    resourceContext.log().debug("Put success: " + payload);
                                    if (result == 200) {
                                        // updated
                                        resourceContext.log().info("Update dataset: Dataset {} updated.", datasetId);
                                    } else {
                                        // created
                                        resourceContext.log().info("Create dataset: Dataset {} created.", datasetId);
                                    }
                                    promise.complete(
                                            ReturnHelper.returnSuccess(result, new JsonObject().put("id", datasetId)));
                                }).onFailure(failure -> {
                                    resourceContext.log().error("Put failed: " +
                                            failure.getMessage());
                                    promise.fail(failure);
                                });
                    }).onFailure(promise::fail);
                }).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<JsonObject> modifyDataset(String datasetId, JsonObject payload) {
        PiveauContext resourceContext = serviceContext.extend(getResourceContext(datasetId, payload));
        DateChecker.check(payload);
        SpatialChecker.check(payload);
        AtomicReference<JsonObject> content = new AtomicReference<>(payload);

        return Future.<JsonObject>future(promise -> {
                    if (payload.containsKey("catalog")) {
                        cataloguesService.checkCatalogueInPayloadObject(payload).onComplete(promise);
                    } else {
                        promise.complete(payload);
                    }
                })
                .compose(vocabularyService::replaceVocabularyInPayload)
                .compose(result -> {
                    content.set(result);
                    return searchClient.getDocument(Constants.DOC_TYPE_DATASET,
                            Constants.getReadAlias(Constants.DOC_TYPE_DATASET), datasetId, true);
                })
                .compose(result -> {
                    hvdSetter.setWithPatch(result, content.get());
                    return createDatasetRevisionDEPRECATED(datasetId, result)
                            .onFailure(cause -> resourceContext.log().error("Write dataset revision", cause));
                })
                .compose(v -> searchClient.patchDocument(Constants.DOC_TYPE_DATASET, datasetId, true, content.get()))
                .map(v -> {
                    resourceContext.log().debug("Patch success: {}", content.get());
                    return ReturnHelper.returnSuccess(200, new JsonObject().put("id", datasetId));
                })
                .onFailure(cause -> resourceContext.log().error("Patch failed: {}", cause.getMessage()));
    }

    @Override
    public Future<JsonObject> readDataset(String datasetId) {
        return searchClient.getDocument(Constants.DOC_TYPE_DATASET,
                        Constants.getReadAlias(Constants.DOC_TYPE_DATASET), datasetId, true)
                .compose(result -> cataloguesService.replaceCatalogueInResponse(result, Collections.emptyList()))
                .map(result -> ReturnHelper.returnSuccess(200, result));
    }

    @Override
    public Future<Void> deleteDataset(String datasetId) {
        Future<JsonObject> readDatasetFuture = readDataset(datasetId);

        return readDatasetFuture.compose(dataset -> {
            JsonArray revisions = dataset
                    .getJsonObject("result")
                    .getJsonObject("catalog_record")
                    .getJsonArray("revisions");
            // Directly delete dataset if it has no revision
            if (Helper.isNullOrEmpty(revisions)) {
                return searchClient.deleteDocument(Constants.DOC_TYPE_DATASET, datasetId, true);
            }

            // If it does, create a list to hold the futures for deleting revisions
            List<Future<Void>> deleteRevisionFutures = new ArrayList<>();
            for (int i = 0; i < revisions.size(); i++) {
                String revisionId = revisions
                        .getJsonObject(i)
                        .getString("id");
                deleteRevisionFutures.add(deleteDatasetRevision(revisionId));
            }
            // Delete dataset and its revisions
            return Future.all(deleteRevisionFutures).recover(t -> {
                if (t instanceof ServiceException exception && exception.failureCode() == 404) {
                    // Return success if one or more dataset-revision in catalog_record.revisions cannot be found
                    return Future.succeededFuture();
                }
                return Future.failedFuture(t);
            }).compose(v -> searchClient.deleteDocument(Constants.DOC_TYPE_DATASET, datasetId, true));
        });
    }

    @Override
    public Future<JsonArray> createOrUpdateDatasetBulk(JsonArray payload) {
        PiveauContext resourceContext = serviceContext.extend("bulk");

        DateChecker.check(payload);
        SpatialChecker.check(payload);
        hvdSetter.set(payload);

        return cataloguesService.checkCatalogueInPayloadArray(payload)
                .compose(vocabularyService::replaceVocabularyInPayloadList)
                .compose(content -> searchClient.putDocumentsBulk(
                        Constants.DOC_TYPE_DATASET,
                        "dataset-revisions",
                        List.of("quality_meas"),
                        content,
                        true))
                .onSuccess(result -> resourceContext.log().debug("Put bulk success: {}", result))
                .onFailure(failure -> resourceContext.log().error("Put bulk failed", failure));
    }

    @Override
    public Future<JsonObject> getDatasetRSS(String datasetId) {
        Promise<JsonObject> promise = Promise.promise();

        searchClient.getDocument(Constants.DOC_TYPE_DATASET,
                        Constants.getReadAlias(Constants.DOC_TYPE_DATASET), datasetId, true)
                .onSuccess(datasetJson -> {
                    getRevisionIndices().onComplete(indicesList -> {
                        if (indicesList.succeeded()) {
                            JsonArray revisions = new JsonArray();
                            revisions.add(datasetJson);
                            List<Future<Void>> futureList = new ArrayList<>();

                            indicesList.result().forEach(index -> {
                                Promise<Void> indexPromise = Promise.promise();
                                readDatasetRevision(datasetId, index).onComplete(asyncResult -> {
                                    if (asyncResult.succeeded()) {
                                        revisions.add(asyncResult.result().getJsonObject("result"));
                                    }
                                    indexPromise.complete();
                                });
                                futureList.add(indexPromise.future());
                            });

                            Future.all(futureList)
                                    .onFailure(promise::fail)
                                    .onSuccess(rev -> {
                                        JsonObject object = new JsonObject();
                                        object.put("status", 200);
                                        object.put("id", datasetJson.getValue("id"));
                                        object.put("title", datasetJson.getValue("title"));
                                        object.put("translation_meta", datasetJson.getJsonObject("translation_meta"));
                                        object.put("result", revisions);

                                        promise.complete(object);
                                    });
                        } else {
                            promise.fail(indicesList.cause());
                        }
                    });
                }).onFailure(promise::fail);

        return promise.future();
    }

    @Override
    public Future<JsonObject> readDatasetRevision(String datasetId, String revision) {
        Promise<JsonObject> promise = Promise.promise();
        searchClient.getDocument(Constants.DOC_TYPE_DATASET, revision, datasetId, true).onSuccess(result -> {
            result.put("revision", revision);
            cataloguesService.replaceCatalogueInResponse(result, Collections.emptyList())
                    .onSuccess(replaceCatalogResult ->
                            promise.complete(ReturnHelper.returnSuccess(200, replaceCatalogResult)))
                    .onFailure(promise::fail);
        }).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<JsonObject> createNewDatasetRevision(String datasetId, String revisionId) {
        String datasetAlias = Constants.getReadAlias(Constants.DOC_TYPE_DATASET);

        return searchClient.getDocumentById(datasetAlias, datasetId, true)
                .compose(getResponse -> {
                    if (getResponse.found()) {
                        return Future.succeededFuture(getResponse.source());
                    } else {
                        log.error("Create dataset-revision: dataset {} is not found", datasetId);
                        return Future.failedFuture(new ServiceException(404, "Dataset " + datasetId + " not found"));
                    }
                })
                // TODO: check if a revision with the given id is already exist
                .compose(source -> turnDatasetIntoRevision(source, revisionId))
                .compose(revision -> searchClient.indexDocument(Constants.DOC_TYPE_DATASETREVISIONS, revisionId, revision))
                .map(result -> {
                    log.info("Create dataset-revision: Revision {} is created for dataset {}", revisionId, datasetId);
                    return ReturnHelper.returnSuccess(201, new JsonObject().put("id", result));
                })
                .recover(err -> {
                    log.error("Create dataset-revision: Failed, {}", err.getMessage());
                    return Future.failedFuture(err);
                });
    }

    @Override
    public Future<JsonObject> readDatasetRevisionById(String revisionId) {
        Promise<JsonObject> promise = Promise.promise();

        getRevisionIndices().onComplete(indicesList -> {
            if (indicesList.succeeded()) {
                Set<String> indices = indicesList.result();
                searchInNextIndex(indices.iterator(), Constants.DOC_TYPE_DATASETREVISIONS, revisionId, promise);
            } else {
                promise.fail(indicesList.cause());
            }
        });

        return promise.future();
    }

    private void searchInNextIndex(Iterator<String> iterator, String docType, String documentId, Promise<JsonObject> promise) {
        if (!iterator.hasNext()) {
            promise.fail(new ServiceException(404, "Document not found"));
            return;
        }

        searchClient.getDocument(docType, iterator.next(), documentId, true)
                .onSuccess(result -> promise.complete(ReturnHelper.returnSuccess(200, result)))
                .onFailure(ar -> searchInNextIndex(iterator, docType, documentId, promise));
    }

    @Override
    public Future<Void> deleteDatasetRevision(String revisionId) {
        return searchClient.deleteDocument("dataset-revisions", revisionId, true);
    }

    @Override
    public Future<String> triggerSyncScores() {
        Promise<String> promise = Promise.promise();

        PiveauContext resourceContext = serviceContext.extend("dataset");

        JsonArray includes = new JsonArray();
        includes.add("id");
        includes.add("quality_meas");

        JsonObject searchParams = new JsonObject();
        searchParams.put("minScoring", 0);

        int size = 500;

        JsonObject q = new JsonObject();
        q.put("size", size);
        q.put("filter", "dataset");
        q.put("aggregation", false);
        q.put("includes", includes);
        q.put("scroll", true);
        q.put("searchParams", searchParams);

        AtomicInteger atomicInteger = new AtomicInteger();
        searchService.search(q.toString()).onComplete(searchQueryResult -> {
            if (searchQueryResult.succeeded()) {
                JsonObject searchResultJson = searchQueryResult.result().getJsonObject("result");
                int count = searchResultJson.getInteger("count");
                String scrollId = searchResultJson.getString("scrollId");
                JsonArray searchResult = searchResultJson.getJsonArray("results");
                modifyScores(resourceContext, searchResult).onComplete(modifyScoresResult -> {
                    if (modifyScoresResult.succeeded()) {
                        for (int i = 1; i < (count / size) + 1; i++) {
                            vertx.setTimer(i * 1000L, ar ->
                                    searchService.scroll(scrollId).onComplete(scrollQueryResult -> {
                                        if (scrollQueryResult.succeeded()) {
                                            JsonObject scrollResultJson =
                                                    scrollQueryResult.result().getJsonObject("result");
                                            JsonArray scrollResult = scrollResultJson.getJsonArray("results");
                                            modifyScores(resourceContext, scrollResult).onComplete(modifyScores2Result -> {
                                                if (modifyScores2Result.failed()) {
                                                    resourceContext.log().error("Sync score failed: "
                                                            + modifyScoresResult.cause().getMessage());
                                                }
                                                resourceContext.log().info("SyncScore processed: " +
                                                        atomicInteger.incrementAndGet() + "; Size: " + scrollResult.size());
                                            });
                                        } else {
                                            resourceContext.log().error("Sync score failed: "
                                                    + modifyScoresResult.cause().getMessage());
                                        }
                                    })
                            );
                        }
                    } else {
                        resourceContext.log().error("Sync score failed: " + modifyScoresResult.cause().getMessage());
                    }
                    resourceContext.log().info("SyncScore processed: "
                            + atomicInteger.incrementAndGet() + "; Size: " + searchResult.size());
                });
            } else {
                resourceContext.log().error("Sync score failed: " + searchQueryResult.cause().getMessage());
            }
        });
        promise.complete("Triggered sync scores successfully");
        return promise.future();
    }

    private Future<JsonObject> createDatasetRevisionDEPRECATED(String datasetId, JsonObject payload) {
        PiveauContext resourceContext = serviceContext.extend(getResourceContext(datasetId, payload));
        return searchClient.putDocument("dataset-revisions", datasetId, true, payload).map(result -> {
            if (result == 200) {
                // updated
                resourceContext.log().info("Update dataset revision: Dataset {} updated.", datasetId);
            } else {
                // created
                resourceContext.log().info("Create dataset revision: Dataset {} created.", datasetId);
            }
            return ReturnHelper.returnSuccess(result, new JsonObject().put("id", datasetId));
        });
    }

    private Future<Void> modifyScores(PiveauContext resourceContext, JsonArray searchResult) {
        List<Future<Void>> futureList = new ArrayList<>();
        for (Object obj : searchResult) {
            Promise<Void> searchResultPromise = Promise.promise();
            futureList.add(searchResultPromise.future());

            JsonObject dataset = (JsonObject) obj;
            String id = dataset.getString("id");
            JsonObject qualityMeas = dataset.getJsonObject("quality_meas");

            if (qualityMeas != null && !qualityMeas.isEmpty()) {
                modifyDataset(id, new JsonObject().put("quality_meas", qualityMeas)).onComplete(modifyDatasetResult -> {
                    if (modifyDatasetResult.failed()) {
                        resourceContext.extend(id).log().error("Patch score failed: "
                                + modifyDatasetResult.cause().getMessage());
                    }
                    searchResultPromise.complete();
                });
            } else {
                searchResultPromise.complete();
            }
        }
        return Future.all(futureList).mapEmpty();
    }

    private Future<Set<String>> getRevisionIndices() {
        return searchClient.getAliases("dataset-revisions_");
    }

    private Future<JsonData> turnDatasetIntoRevision(JsonData dataset, String revisionId) {
        Map<String, Object> revision = dataset.to(LinkedHashMap.class.getGenericSuperclass());
        Map<String, Object> catalogRecord = (Map<String, Object>) revision.get("catalog_record");

        // Modify id
        revision.put("id", revisionId);
        // Modify catalog_record
        if (catalogRecord != null) {
            catalogRecord.remove("visibility");
            catalogRecord.remove("revisions");
        }

        return Future.succeededFuture(JsonData.of(revision));
    }

}
