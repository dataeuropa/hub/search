package io.piveau.hub.search.services.search;

import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.services.catalogues.CataloguesService;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.piveau.utils.PiveauContext;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the search service
 */

public class SearchServiceImpl implements SearchService {
    private static final Logger LOG = LoggerFactory.getLogger(SearchServiceImpl.class);
    private final IndexManager indexManager;

    private final SearchClient searchClient;

    private final PiveauContext serviceContext;

    private final CataloguesService cataloguesService;

    // cache for scroll ids
    private final Cache<String, QueryParams> cacheScrollIds;

    // caches for facet titles
    private final Cache<String, String> cacheTitleString;
    private final Cache<String, JsonObject> cacheTitleJson;

    private static final String FACETS = "facets";
    private static final String RESULT = "result";

    SearchServiceImpl(Vertx vertx, JsonObject config, IndexManager indexManager, Handler<AsyncResult<SearchService>> handler) {
        this.indexManager = indexManager;

        this.searchClient = SearchClient.build(vertx, config, indexManager);

        this.cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);

        this.serviceContext = new PiveauContext("hub.search", "SearchService");

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().withCache("scrollIdData",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, QueryParams.class,
                                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(10000, EntryUnit.ENTRIES))
                                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(10))))
                .build(true);

        cacheScrollIds = cacheManager.getCache("scrollIdData", String.class, QueryParams.class);

        CacheManager cacheManagerString = CacheManagerBuilder.newCacheManagerBuilder().withCache("titleDataString",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(10000, EntryUnit.ENTRIES))
                                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cacheTitleString = cacheManagerString.getCache("titleDataString", String.class, String.class);

        CacheManager cacheManagerJson = CacheManagerBuilder.newCacheManagerBuilder().withCache("titleDataJson",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(10000, EntryUnit.ENTRIES))
                                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cacheTitleJson = cacheManagerJson.getCache("titleDataJson", String.class, JsonObject.class);

        searchClient.ping().onSuccess(result -> {
            for (String index : indexManager.getIndexList()) {
                if (!index.equals("vocabulary")) this.initIndex(index);
            }
            handler.handle(Future.succeededFuture(this));
        }).onFailure(failure -> handler.handle(Future.failedFuture(failure)));
    }

    private void processSingleResult(Promise<Void> replaceCatalogueInResultListPromise, Promise<Void> replaceTitleInItemsPromise,
                                     Promise<Void> replaceCatalogueInItemsPromise, JsonObject result, QueryParams query) {
        JsonArray facets = result.getJsonArray(FACETS);
        String index = result.getString("index");

        if (query.isScroll())
            cacheScrollIds.put(result.getString("scrollId"), query);

        List<String> includesRevised = new ArrayList<>();
        List<String> includes = query.getIncludes();
        if (!Helper.isNullOrEmpty(includes)) {
            includes.stream()
                    .filter(include -> include.contains("catalog."))
                    .map(include -> include.replace("catalog.", ""))
                    .forEach(includesRevised::add);
        }

        cataloguesService.replaceCatalogueInResultList(result.getJsonArray("results"), includesRevised)
                .onSuccess(ar -> result.put("results", ar))
                .onComplete(ar -> replaceCatalogueInResultListPromise.complete());

        if (Helper.isNullOrEmpty(facets)) {
            replaceTitleInItemsPromise.complete();
            replaceCatalogueInItemsPromise.complete();
            return;
        }
        replaceTitleInFacets(index, facets)
                .onComplete(replaceTitleInFacetsResult -> {
                    replaceTitleInItemsPromise.complete();
                    boolean found = facets.stream()
                            .map(JsonObject.class::cast)
                            .anyMatch(facet -> {
                                if ("catalog".equals(facet.getString("id"))) {
                                    if (Helper.isNullOrEmpty(facet.getJsonArray(Constants.FACET_ITEMS))) {
                                        replaceCatalogueInItemsPromise.complete();
                                    } else {
                                        cataloguesService.replaceCatalogueInItems(facet.getJsonArray(Constants.FACET_ITEMS))
                                                .onSuccess(ar -> facet.put(Constants.FACET_ITEMS, ar))
                                                .onComplete(ar -> replaceCatalogueInItemsPromise.complete());
                                    }
                                    return true; // a match found
                                }
                                return false; // no match
                            });
                    if (!found) {
                        replaceCatalogueInItemsPromise.complete();
                    }
                });
    }

    private Future<JsonObject> handleSearchResult(JsonObject searchResult, QueryParams query) {
        Object resultObj = searchResult.getValue(RESULT);
        if (resultObj instanceof JsonObject) {
            JsonObject result = searchResult.getJsonObject(RESULT);

            return handleSingleResult(result, query)
                    .map(v -> searchResult); // on success
        } else if (resultObj instanceof JsonArray) {
            JsonArray resultArr = searchResult.getJsonArray(RESULT);
            List<Future> futures = new ArrayList<>();
            for (int i = 0; i < resultArr.size(); i++) {
                JsonObject result = resultArr.getJsonObject(i);
                futures.add(handleSingleResult(result, query));
            }

            return CompositeFuture.all(futures)
                    .map(v -> searchResult) // on success
                    .recover(err -> {
                        LOG.error(err.getCause().getMessage());
                        return Future.failedFuture(err.getCause().getMessage());
                    }); // on failure
        } else {
            return Future.succeededFuture(searchResult);
        }
    }

    private Future<Void> handleSingleResult(JsonObject result, QueryParams query) {
        Promise<Void> replaceCatalogueInResultListPromise = Promise.promise();
        Promise<Void> replaceTitleInItemsPromise = Promise.promise();
        Promise<Void> replaceCatalogueInItemsPromise = Promise.promise();

        processSingleResult(replaceCatalogueInResultListPromise, replaceTitleInItemsPromise,
                replaceCatalogueInItemsPromise, result, query);

        return CompositeFuture.all(List.of(replaceCatalogueInResultListPromise.future(),
                        replaceTitleInItemsPromise.future(),
                        replaceCatalogueInItemsPromise.future()))
                .mapEmpty();
    }

    @Override
    public Future<JsonObject> search(String q) {
        Promise<JsonObject> promise = Promise.promise();
        QueryParams query = Json.decodeValue(q, QueryParams.class);

        searchClient.search(query)
                .onSuccess(searchResult -> handleSearchResult(searchResult, query)
                        .onSuccess(promise::complete)
                        .onFailure(promise::fail))
                .onFailure(promise::fail);

        return promise.future();
    }

    @Override
    public Future<JsonObject> scroll(String scrollId) {
        Promise<JsonObject> promise = Promise.promise();
        QueryParams query = cacheScrollIds.get(scrollId);
        if (query != null) {
            searchClient.scroll(scrollId)
                    .onSuccess(searchResult -> handleSearchResult(searchResult, query)
                            .onSuccess(promise::complete)
                            .onFailure(promise::fail))
                    .onFailure(promise::fail);
        } else {
            promise.fail(new ServiceException(404, "ScrollId not found"));
        }
        return promise.future();
    }

    @Override
    public Future<Boolean> indexExists(String index) {
        Promise<Boolean> promise = Promise.promise();
        searchClient.indexExists(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> indexCreate(String index, Integer numberOfShards) {
        Promise<String> promise = Promise.promise();
        searchClient.indexCreate(index, numberOfShards).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> indexDelete(String index) {
        Promise<String> promise = Promise.promise();
        searchClient.indexDelete(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> indexReset() {
        Promise<String> promise = Promise.promise();
        for (String index : indexManager.getIndexList()) {
            if (index.equals("vocabulary")) indexDelete("vocabulary_*").onComplete(indexDeleteResult -> {
            });
            else indexDelete(index + "*").onComplete(indexDeleteResult -> initIndex(index));
        }
        promise.complete("Triggered index reset.");
        return promise.future();
    }

    @Override
    public Future<String> putMapping(String index) {
        Promise<String> promise = Promise.promise();
        searchClient.putMapping(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> setIndexAlias(String oldIndex, String newIndex, String alias) {
        Promise<String> promise = Promise.promise();
        searchClient.setIndexAlias(oldIndex, newIndex, alias).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> boost(String index, String field, Float value) {
        Promise<String> promise = Promise.promise();
        indexManager.boost(index, field, value, boostResult -> {
            if (boostResult.succeeded()) {
                promise.complete(boostResult.result());
            } else {
                promise.fail(boostResult.cause());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> setMaxAggSize(String index, Integer maxAggSize) {
        Promise<String> promise = Promise.promise();
        indexManager.setMaxAggSize(index, maxAggSize, setMaxAggSizeResult -> {
            if (setMaxAggSizeResult.succeeded()) {
                promise.complete(setMaxAggSizeResult.result());
            } else {
                promise.fail(setMaxAggSizeResult.cause());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> setMaxResultWindow(String index, Integer maxResultWindow) {
        Promise<String> promise = Promise.promise();
        searchClient.setMaxResultWindow(index, maxResultWindow).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> setNumberOfReplicas(String index, Integer numberOfReplicas) {
        Promise<String> promise = Promise.promise();
        searchClient.setNumberOfReplicas(index, numberOfReplicas).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> putIndexTemplate(String index) {
        Promise<String> promise = Promise.promise();
        searchClient.putIndexTemplate(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> putLifecyclePolicy(String index) {
        Promise<String> promise = Promise.promise();
        searchClient.putLifecyclePolicy(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<JsonArray> getIndices(String index) {
        Promise<JsonArray> promise = Promise.promise();
        searchClient.getIndices(index).onSuccess(promise::complete).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<JsonArray> listIds(String filter, String field, JsonArray terms, String alias) {
        Promise<JsonArray> promise = Promise.promise();

        JsonObject q = new JsonObject();
        q.put("size", 10000);
        q.put("filter", filter);
        q.put("aggregation", false);
        q.put("includes", new JsonArray().add("id"));
        q.put("scroll", true);
        q.put("alias", alias);

        if (field != null && !field.isEmpty() && terms != null && !terms.isEmpty()) {
            q.put(FACETS, new JsonObject().put(field, terms));
        }

        QueryParams query = Json.decodeValue(q.toString(), QueryParams.class);

        searchClient.listIds(query, false, true).onSuccess(promise::complete).onFailure(promise::fail);

        return promise.future();
    }

    private void initMapping(String index) {
        putMapping(index + "_write").onComplete(putMappingResult -> {
            if (putMappingResult.succeeded()) {
                setMaxResultWindow(index + "_write", indexManager.getMaxResultWindow().get(index)).onComplete(
                        setMaxResultWindowResult -> {
                            if (!setMaxResultWindowResult.succeeded()) {
                                serviceContext.log().error(setMaxResultWindowResult.cause().getMessage());
                            }
                        });
            } else {
                serviceContext.log().error(putMappingResult.cause().getMessage());
            }
        });
    }

    private void initIndex(String index) {
        indexExists(index + "_*").onComplete(indexExistsResult -> {
            if (!indexExistsResult.succeeded()) return;

            Promise<String> p = Promise.promise();
            if (indexManager.lifecyclePolicyExists(index)) {
                putLifecyclePolicy(index).onComplete(r -> p.complete(r.result()));
            } else {
                p.complete();
            }

            p.future().compose(r -> {
                Promise<String> tp = Promise.promise();
                if (indexManager.indexTemplateExist(index)) {
                    putIndexTemplate(index).onComplete(s -> tp.complete(s.result()));
                } else {
                    tp.complete();
                }
                return tp.future();
            })
            .onFailure(err -> serviceContext.log().error(err.getMessage()))
            .onSuccess(r -> {
                if (Boolean.FALSE.equals(indexExistsResult.result())) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
                    Date today = new Date(System.currentTimeMillis());

                    String indexToday = index + "_" + formatter.format(today) + "-000001";
                    indexCreate(indexToday, null).onComplete(indexCreateResult -> {
                        if (!indexCreateResult.succeeded()) {
                            serviceContext.log().error(indexCreateResult.cause().getMessage());
                        }

                        setIndexAlias(null, indexToday, index + Constants.ELASTIC_WRITE_ALIAS).onComplete(
                                setIndexWriteAliasResult -> {
                                    if (!setIndexWriteAliasResult.succeeded()) {
                                        serviceContext.log().error(setIndexWriteAliasResult.cause().getMessage());
                                    }
                                    setIndexAlias(null, indexToday, index + Constants.ELASTIC_READ_ALIAS).onComplete(
                                            setIndexReadAliasResult -> {
                                                if (!setIndexReadAliasResult.succeeded()) {
                                                    serviceContext.log().error(
                                                            setIndexReadAliasResult.cause().getMessage());
                                                }
                                                initMapping(index);
                                            });
                                });
                    });
                } else {
                    initMapping(index);
                }
            });
        });
    }

    /*
     * F A C E T S
     */

    private Future<Void> replaceTitleInFacets(String index, JsonArray facets) {
        if (Helper.isNullOrEmpty(index) || ("*").equals(index)) {
            return Future.succeededFuture();
        }

        List<Future<Void>> futureList = new ArrayList<>();
        for (Object obj : facets) {
            JsonObject facet = (JsonObject) obj;
            String facetId = facet.getString("id");
            if ("catalog".equals(facetId)) {
                futureList.add(Future.succeededFuture());
            } else {
                String type = indexManager.getFacets().get(index).get(facetId).getString("type");
                if (type != null && !type.isEmpty() && !type.equals("term")) {
                    futureList.add(Future.succeededFuture());
                } else {
                    JsonArray facetItems = facet.getJsonArray(Constants.FACET_ITEMS);
                    futureList.add(replaceTitleInFacetItems(index, facetId, facetItems)
                            .onFailure(err -> LOG.error(
                                    "Failed to replace title in facet {}. ERROR: {}", facetId, err.getCause().getMessage())
                            ));
                }
            }
        }

        return Future.all(futureList).mapEmpty();
    }

    private Future<Void> replaceTitleInFacetItems(String index, String facetId, JsonArray items) {
        if (Helper.isNullOrEmpty(items)) {
            return Future.succeededFuture();
        }

        List<Future<Void>> futureList = items.stream()
                .filter(item -> (item instanceof JsonObject) && !((JsonObject) item).isEmpty())
                .map(item -> {
                    JsonObject toReplace = (JsonObject) item;
                    String itemId = toReplace.getString("id");
                    return readFacetTitle(index, facetId, itemId)
                            .compose(result -> {
                                Object readTitle = result.getValue(RESULT);
                                toReplace.put("title", readTitle);
                                return Future.<Void>succeededFuture();
                            }).onFailure(err -> LOG.error(
                                    "Failed to read facet {}. ERROR: {}", facetId, err.getCause().getMessage())
                            );
                })
                .toList();

        return Future.all(futureList).mapEmpty();
    }

    private Future<JsonObject> readFacetTitle(String index, String facetId, String itemId) {
        if (Helper.isNullOrEmpty(itemId)) {
            return Future.failedFuture(new ServiceException(400, "ID is null or empty"));
        }

        String cacheKey = index + "_" + facetId + "_" + itemId;
        if (cacheTitleString.containsKey(cacheKey)) {
            return Future.succeededFuture(ReturnHelper.returnSuccess(200, cacheTitleString.get(cacheKey)));
        } else if (cacheTitleJson.containsKey(cacheKey)) {
            return Future.succeededFuture(ReturnHelper.returnSuccess(200, cacheTitleJson.get(cacheKey)));
        } else {
            JsonObject facetJson = indexManager.getFacets().get(index).get(facetId);

            String path = facetJson.getString("path");
            String displayTitle = facetJson.getString("display_title", "label");

            final String fromIndex = facetJson.getString("fromIndex", "");
            if (!fromIndex.isEmpty()) {
                path = displayTitle;
                facetId = "resource";
                index = fromIndex;
            }

            JsonObject facets = new JsonObject()
                    .put(facetId, new JsonArray().add(itemId));

            JsonArray includes = new JsonArray().add(path);

            JsonObject q = new JsonObject();
            q.put("filter", index);
            q.put(FACETS, facets);
            q.put("includes", includes);
            q.put("aggregation", false);
            q.put("from", 0);
            q.put("size", 1);

            QueryParams query = Json.decodeValue(q.toString(), QueryParams.class);

            return searchClient.searchFacetTitle(query, itemId, facetId, !fromIndex.isEmpty())
                    .compose(result -> {
                        if (result instanceof String resultString) {
                            cacheTitleString.put(cacheKey, resultString);
                            return Future.succeededFuture(ReturnHelper.returnSuccess(200, resultString));
                        } else if (result instanceof JsonObject resultJsonObject) {
                            cacheTitleJson.put(cacheKey, resultJsonObject);
                            return Future.succeededFuture(ReturnHelper.returnSuccess(200, resultJsonObject));
                        } else {
                            return Future.failedFuture(new ServiceException(500, "Title must be string or json"));
                        }
                    });
        }
    }

}
