package io.piveau.hub.search.services.sitemaps;

import io.piveau.hub.search.Constants;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.json.ConfigHelper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;

public class SitemapsServiceVerticle extends AbstractVerticle {

    private final IndexManager indexManager;

    public SitemapsServiceVerticle(IndexManager indexManager) {
        this.indexManager = indexManager;
    }

    @Override
    public void start(Promise<Void> startPromise) {
        JsonObject esConfig = ConfigHelper.forConfig(config())
                .forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_ES_CONFIG);
        JsonObject sitemapConfig = ConfigHelper.forConfig(config())
                .forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_SITEMAP_CONFIG);

        SitemapsService.create(vertx, esConfig, indexManager, sitemapConfig).onSuccess(result -> {
            new ServiceBinder(vertx).setAddress(SitemapsService.SERVICE_ADDRESS)
                    .register(SitemapsService.class, result);
            startPromise.complete();
        }).onFailure(startPromise::fail);
    }
}
