package io.piveau.hub.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Constants {

    public static final String ENV_PIVEAU_HUB_SEARCH_API_KEY = "PIVEAU_HUB_SEARCH_API_KEY";
    public static final String ENV_PIVEAU_HUB_SEARCH_SERVICE_PORT = "PIVEAU_HUB_SEARCH_SERVICE_PORT";
    public static final String ENV_PIVEAU_HUB_SEARCH_CLI_CONFIG = "PIVEAU_HUB_SEARCH_CLI_CONFIG";
    public static final String ENV_PIVEAU_HUB_SEARCH_ES_CONFIG = "PIVEAU_HUB_SEARCH_ES_CONFIG";
    public static final String ENV_PIVEAU_HUB_SEARCH_SITEMAP_CONFIG = "PIVEAU_HUB_SEARCH_SITEMAP_CONFIG";
    public static final String ENV_PIVEAU_HUB_SEARCH_GAZETTEER_CONFIG = "PIVEAU_HUB_SEARCH_GAZETTEER_CONFIG";
    public static final String ENV_PIVEAU_HUB_SEARCH_FEED_CONFIG = "PIVEAU_HUB_SEARCH_FEED_CONFIG";
    public static final String ENV_PIVEAU_HUB_SEARCH_HTTP_BODY_LIMIT = "PIVEAU_HUB_SEARCH_HTTP_BODY_LIMIT";
    public static final String ENV_PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA = "PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA";
    public static final String ENV_PIVEAU_FEATURE_FLAGS = "PIVEAU_FEATURE_FLAGS";

    public static final String ENV_PIVEAU_IMPRINT_URL = "PIVEAU_IMPRINT_URL";
    public static final String ENV_PIVEAU_PRIVACY_URL = "PIVEAU_PRIVACY_URL";
    public static final String ENV_PIVEAU_FAVICON_PATH = "PIVEAU_FAVICON_PATH";
    public static final String ENV_PIVEAU_LOGO_PATH = "PIVEAU_LOGO_PATH";
    public static final String ENV_PIVEAU_WEBROOT_PATH = "PIVEAU_WEBROOT_PATH";
    public static final String ENV_PIVEAU_HUB_CORS_DOMAINS = "PIVEAU_HUB_CORS_DOMAINS";
    public static final String ENV_PIVEAU_HVD_SETTER_LEVEL = "PIVEAU_HVD_SETTER_LEVEL";

    public static final String FACET_ITEMS = "items";

    public enum Operator {
        AND,
        OR
    }

    public static final String ELASTIC_READ_ALIAS = "_read";
    public static final String ELASTIC_WRITE_ALIAS = "_write";

    public static final String SEARCH_RESULT_INDEX_FIELD = "index";
    public static final String SEARCH_RESULT_COUNT_FIELD = "count";
    public static final String SEARCH_RESULT_SCROLL_FIELD = "scrollId";
    public static final String SEARCH_RESULT_FACETS_FIELD = "facets";
    public static final String SEARCH_RESULT_RESULTS_FIELD = "results";

    public static final List<Integer> SECURE_WITH_CIRCUIT_BREAKER = new ArrayList<>(Arrays.asList(502, 503));

    public static String getReadAlias(String index) {
        return index + ELASTIC_READ_ALIAS;
    }

    public static String getWriteAlias(String index) {
        return index + ELASTIC_WRITE_ALIAS;
    }

    public static String getIndexType(String index) {
        return index.substring(index.indexOf("_"));
    }

    public static final String DOC_TYPE_CATALOGUE = "catalogue";
    public static final String DOC_TYPE_DATASERVICE = "dataservice";
    public static final String DOC_TYPE_DATASET = "dataset";
    public static final String DOC_TYPE_DATASETREVISIONS = "dataset-revisions";
    public static final String DOC_TYPE_VOCABULARY = "vocabulary";
    public static final String DOC_TYPE_RESOURCE = "resource";

}
