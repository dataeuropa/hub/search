package io.piveau.hub.search.handler;

import io.piveau.hub.search.Constants;
import io.piveau.hub.search.security.*;
import io.piveau.hub.search.services.search.SearchService;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * A handler that manages search request
 */
public class SearchHandler extends ContextHandler {

    private static final Logger LOG = LoggerFactory.getLogger(SearchHandler.class);
    private static final String RESOURCE = "resource";
    SearchService searchService;

    public SearchHandler(Vertx vertx, String address) {
        searchService = SearchService.createProxy(vertx, address);
    }

    JsonObject paramsToQuery(MultiMap params) {
        final String AUTOCOMPLETE = "autocomplete";
        final String SCROLL = "scroll";
        final String Q = "q";
        final String SHOW_SCORE = "showScore";
        final String AGGREGATION = "aggregation";
        final String AGGREGATION_FIELDS = "aggregationFields";
        final String AGGREGATION_ALL_FIELDS = "aggregationAllFields";
        final String GLOBAL_AGGREGATION = "globalAggregation";
        final String ONLY_IDS = "onlyIds";
        final String FACET_OPERATOR = "facetOperator";
        final String FACET_GROUP_OPERATOR = "facetGroupOperator";
        final String FILTER_DISTRIBUTIONS = "filterDistributions";
        final String SORT = "sort";
        final String FIELDS = "fields";
        final String INCLUDES = "includes";
        final String EXIST = "exists";
        final String FILTERS = "filters";
        final String FACETS = "facets";

        JsonObject query = new JsonObject();

        String q = params.get(Q);
        params.remove(Q);
        query.put(Q, q);

        int page;
        try {
            page = Integer.parseInt(params.get("page"));
        } catch (NumberFormatException e) {
            page = 0;
        }
        params.remove("page");

        int limit;
        try {
            limit = Integer.parseInt(params.get("limit"));
        } catch (NumberFormatException e) {
            limit = 10;
        }
        params.remove("limit");

        query.put("from", page*limit);
        query.put("size", limit);

        String scroll = params.get(SCROLL);
        params.remove(SCROLL);
        query.put(SCROLL, scroll);

        String showScore = params.get(SHOW_SCORE);
        params.remove(SHOW_SCORE);
        query.put(SHOW_SCORE, showScore);

        String aggregation = params.get(AGGREGATION);
        params.remove(AGGREGATION);
        query.put(AGGREGATION, aggregation);

        String onlyIds = params.get(ONLY_IDS);
        params.remove(ONLY_IDS);
        query.put(ONLY_IDS, onlyIds);

        String globalAggregation = params.get(GLOBAL_AGGREGATION);
        params.remove(GLOBAL_AGGREGATION);
        query.put(GLOBAL_AGGREGATION, globalAggregation);

        String aggregationAllFields = params.get(AGGREGATION_ALL_FIELDS);
        params.remove(AGGREGATION_ALL_FIELDS);
        query.put(AGGREGATION_ALL_FIELDS, aggregationAllFields);

        List<String> aggregationFields = params.getAll(AGGREGATION_FIELDS);
        params.remove(AGGREGATION_FIELDS);
        //if(!aggregationFields.isEmpty())
        query.put(AGGREGATION_FIELDS, new JsonArray(checkCommaDelimited(aggregationFields)));

        String facetOperator = params.get(FACET_OPERATOR);
        params.remove(FACET_OPERATOR);
        query.put(FACET_OPERATOR, facetOperator);

        String facetGroupOperator = params.get(FACET_GROUP_OPERATOR);
        params.remove(FACET_GROUP_OPERATOR);
        query.put(FACET_GROUP_OPERATOR, facetGroupOperator);

        String filterDistributions = params.get(FILTER_DISTRIBUTIONS);
        params.remove(FILTER_DISTRIBUTIONS);
        query.put(FILTER_DISTRIBUTIONS, filterDistributions);

        List<String> sort = params.getAll(SORT);
        params.remove(SORT);
        sort = checkCommaDelimited(sort);
        for (int i = 0; i < sort.size(); i++) {
            sort.set(i, sort.get(i).replaceAll(" ", "+"));
        }
        query.put(SORT, new JsonArray(sort));

        String autocomplete = params.get(AUTOCOMPLETE);
        params.remove(AUTOCOMPLETE);
        query.put(AUTOCOMPLETE, autocomplete);

        // DEPRECATED
        String filter = params.get("filter");
        params.remove("filter");
        if(filter != null) {
            query.put("filter", filter);

            String facets = params.get(FACETS);
            params.remove(FACETS);
            if (facets != null) {
                JsonObject facetsJson;
                try {
                    facetsJson = new JsonObject(facets);
                } catch (DecodeException e) {
                    facetsJson = new JsonObject();
                }
                query.put(FACETS, facetsJson);
            }
        }

        List<String> filters=params.getAll(FILTERS);
        params.remove(FILTERS);
        if(!filters.isEmpty()) {
            List<String> queryFilters = handleFiltersToQuery(filters, params);
            query.put(FILTERS, queryFilters);

            String facets = params.get(FACETS);
            params.remove(FACETS);
            if (facets != null) {
                JsonObject facetsJson;
                try {
                    facetsJson = new JsonObject(facets);
                } catch (DecodeException e) {
                    facetsJson = new JsonObject();
                }
                query.put(FACETS, facetsJson);
            }
        }


        List<String> vocabulary = params.getAll(Constants.DOC_TYPE_VOCABULARY);
        params.remove(Constants.DOC_TYPE_VOCABULARY);
        query.put(Constants.DOC_TYPE_VOCABULARY, new JsonArray(checkCommaDelimited(vocabulary)));

        List<String> resource = params.getAll(RESOURCE);
        params.remove(RESOURCE);
        query.put(RESOURCE, new JsonArray(checkCommaDelimited(resource)));

        List<String> fields = params.getAll(FIELDS);
        params.remove(FIELDS);
        query.put(FIELDS, new JsonArray(checkCommaDelimited(fields)));

        List<String> includes = params.getAll(INCLUDES);
        params.remove(INCLUDES);
        query.put(INCLUDES, new JsonArray(checkCommaDelimited(includes)));

        List<String> exists = params.getAll(EXIST);
        params.remove(EXIST);
        query.put(EXIST, new JsonArray(checkCommaDelimited(exists)));

        // DEPRECATED
        String superCatalogue = params.get("superCatalogue");
        params.remove("superCatalogue");
        query.put("superCatalogue", superCatalogue);

        String minDate = params.get("minDate");
        params.remove("minDate");
        String maxDate = params.get("maxDate");
        params.remove("maxDate");
        String minScoring = params.get("minScoring");
        params.remove("minScoring");
        String maxScoring = params.get("maxScoring");
        params.remove("maxScoring");
        String countryData = params.get("countryData");
        params.remove("countryData");
        String dataServices = params.get("dataServices");
        params.remove("dataServices");

        String bboxMinLon = params.get("bboxMinLon");
        params.remove("bboxMinLon");
        String bboxMaxLon = params.get("bboxMaxLon");
        params.remove("bboxMaxLon");
        String bboxMinLat = params.get("bboxMinLat");
        params.remove("bboxMinLat");
        String bboxMaxLat = params.get("bboxMaxLat");
        params.remove("bboxMaxLat");
        String[] boundingBoxArr = {bboxMinLon, bboxMaxLon, bboxMinLat, bboxMaxLat};

        if( (filter != null && !filter.isEmpty() && !filter.equals(AUTOCOMPLETE)) || !filters.isEmpty() ) {
            JsonObject searchParams = handleSearchParamsToQuery(minDate, maxDate, minScoring, maxScoring, countryData,
                    dataServices, boundingBoxArr);
            query.put("searchParams", searchParams);
        }

        for(Map.Entry<String, String> entry : params.entries()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (key.contains(".")) {
                String[] splitKey = key.split("\\.");
                JsonObject keyObject = query.getJsonObject(splitKey[0], new JsonObject());

                if (splitKey[0].equals("boost")) {
                    try {
                        String boostField = "";
                        for(int i = 1; i < splitKey.length; ++i) {
                            boostField = boostField.concat(splitKey[i]);
                            if (i != splitKey.length-1) {
                                boostField = boostField.concat(".");
                            }
                        }
                        keyObject.put(boostField, Float.valueOf(value));
                    } catch (NumberFormatException e) {
                        continue;
                    }
                } else {
                    keyObject.put(splitKey[1], value);
                }

                query.put(splitKey[0], keyObject);
            }
        }

        // Clean up query
        query.getMap().entrySet().removeIf(entry -> entry.getValue() == null ||
                ( entry.getValue() instanceof JsonArray && (((JsonArray) entry.getValue()).isEmpty()) ) ||
                ( entry.getValue() instanceof JsonObject && ((JsonObject) entry.getValue()).isEmpty())
        );
        return query;
    }

    private List<String> handleFiltersToQuery(List<String> filters, MultiMap params) {
        List<String> queryFilters = new ArrayList<>(checkCommaDelimited(filters));
        List<String> resources = new ArrayList<>(checkCommaDelimited(params.getAll(RESOURCE)));
        if ( queryFilters.contains(RESOURCE) ) {
            if ( !resources.isEmpty() ) {
                for (String resourceType : resources) {
                    queryFilters.add(RESOURCE+"_"+resourceType);
                }
            }
            queryFilters.remove(RESOURCE);
        }

        return queryFilters;
    }

    private JsonObject handleSearchParamsToQuery(String minDate, String maxDate, String minScoring, String maxScoring, String countryData,
                                                 String dataServices, String[] boundingBoxArr) {
        JsonObject searchParams = new JsonObject();

        searchParams.put("minDate", minDate);
        searchParams.put("maxDate", maxDate);
        searchParams.put("minScoring", minScoring);
        searchParams.put("maxScoring", maxScoring);

        if(countryData != null) {
            searchParams.put("countryData", Boolean.parseBoolean(countryData));
        }
        if(dataServices != null) {
            searchParams.put("dataServices", Boolean.parseBoolean(dataServices));
        }

        if(boundingBoxArr[0] != null && boundingBoxArr[1] != null && boundingBoxArr[2] != null && boundingBoxArr[3] != null) {
            JsonObject boundingBox = new JsonObject();
            boundingBox.put("minLon", boundingBoxArr[0]);
            boundingBox.put("maxLon", boundingBoxArr[1]);
            boundingBox.put("minLat", boundingBoxArr[2]);
            boundingBox.put("maxLat", boundingBoxArr[3]);
            searchParams.put("boundingBox", boundingBox);
        }

        searchParams.getMap().entrySet().removeIf(entry -> entry.getValue() == null);
        return searchParams;
    }

    public void searchGet(RoutingContext context) {
        LOG.debug("Search, remote address: {}", context.request().connection().remoteAddress());
        MultiMap params = context.request().params();
        JsonObject query = paramsToQuery(params);

        if (AccessControlUtils.isEnabled()) {
            AccessControlContext accessControlContext = context.get("acCtx");
            accessControlContext.mergeWithQuery(query);
        }

        searchService.search(query.toString()).onComplete(ar -> handleContextLegacy(context, ar));
    }

    public void scrollGet(RoutingContext context) {
        MultiMap params = context.request().params();
        String scrollId = params.get("scrollId");
        searchService.scroll(scrollId).onComplete(ar -> handleContextLegacy(context, ar));
    }

    public void searchPost(RoutingContext context) {
        searchService.search(context.body().asString()).onComplete(ar -> handleContextLegacy(context, ar));
    }

    private List<String> checkCommaDelimited(List<String> input) {
        List<String> output = input;

        if(input.size() == 1)
            output =  Arrays.asList(input.get(0).split(","));

        for (int i = 0; i < output.size(); i++) {
            output.set(i, output.get(i).trim());
        }

        return output;
    }
}
