package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.aggregations.*;
import co.elastic.clients.elasticsearch.core.msearch.MultiSearchResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.ResponseBody;
import co.elastic.clients.json.JsonData;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.request.Field;
import io.piveau.hub.search.util.request.QueryParams;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.*;

public class SearchResponseHelper {

    private static final String DISTRIBUTIONS = "distributions";
    private static final String TITLE = "title";
    private static final String ITEMS = "items";
    private static final String COUNT = "count";
    private static final String DESCRIPTION = "description";

    public static JsonArray simpleProcessSearchResult(ResponseBody<JsonData> searchResponse, String field) {
        JsonArray results = new JsonArray();

        List<Hit<JsonData>> searchHits = searchResponse.hits().hits();
        searchHits.forEach(hit -> {
            Map<String, Object> sourceAsMap = hit.source().to(LinkedHashMap.class.getGenericSuperclass());
            results.add(sourceAsMap.get(field));
        });

        return results;
    }

    public static JsonArray simpleProcessSearchResult(ResponseBody<JsonData> searchResponse) {
        JsonArray results = new JsonArray();

        List<Hit<JsonData>> searchHits = searchResponse.hits().hits();
        searchHits.forEach(hit -> {
            Map<String, Object> sourceAsMap = hit.source().to(LinkedHashMap.class.getGenericSuperclass());
            JsonObject hitResult = new JsonObject(sourceAsMap);
            results.add(hitResult);
        });

        return results;
    }

    public static JsonArray processSearchResult(ResponseBody<JsonData> searchResponse,
                                                QueryParams query, Map<String, JsonObject> datasetFacets,
                                                JsonArray datasets, JsonArray countDatasets,
                                                Map<String, Field> fields) {
        List<Hit<JsonData>> searchHits = searchResponse.hits().hits();

        return addHitResultToResults(searchHits, query, fields, countDatasets, datasets, datasetFacets);
    }

    public static JsonArray processMsearchResult(MultiSearchResponseItem<JsonData> mSearchRespItem,
                                                 QueryParams query, Map<String, JsonObject> datasetFacets,
                                                 JsonArray datasets, JsonArray countDatasets,
                                                 Map<String, Field> fields) {
        JsonArray results = new JsonArray();
        if (mSearchRespItem.isFailure()) {
            return results.add(mSearchRespItem.failure().error().reason());
        } else {
            List<Hit<JsonData>> searchHits = mSearchRespItem.result().hits().hits();
            return addHitResultToResults(searchHits, query, fields, countDatasets, datasets, datasetFacets);
        }
    }

    private static void handleLanguageFields(JsonObject hitResult, String field, List<String> languages) {
        if (!(hitResult.getValue(field) instanceof JsonObject)) return;

        JsonObject fieldJson = hitResult.getJsonObject(field);
        if (!Helper.isNullOrEmpty(fieldJson)) {
            if (!Helper.isNullOrEmpty(languages)) {
                languages.forEach(language -> {
                    if (fieldJson.getString(language) == null) {
                        fieldJson.put(language, fieldJson.getString("_lang"));
                    }
                });
            }
            fieldJson.remove("_lang");
        }
    }

    public static JsonArray processAggregationResult(Integer queryAggregationLimit, Integer queryAggregationMinCount,
                                                     ResponseBody<JsonData> aggregationResponse, List<JsonObject> facetOrder) {
        if (aggregationResponse == null || aggregationResponse.aggregations() == null) return null;

        Map<String, Aggregate> aggregations = aggregationResponse.aggregations();

        return processAggregations(aggregations, queryAggregationLimit, queryAggregationMinCount, facetOrder);
    }

    public static JsonArray processMultipleAggregationResult(Integer queryAggregationLimit, Integer queryAggregationMinCount,
                                                             MultiSearchResponseItem<JsonData> mAggregationResponseItem,
                                                             List<JsonObject> facetOrder) {
        Map<String, Aggregate> aggregations = mAggregationResponseItem.result().aggregations();

        return processAggregations(aggregations, queryAggregationLimit, queryAggregationMinCount, facetOrder);
    }

    public static Object getFacetTitle(JsonObject dataset, String id, String displayId, String displayTitle) {
        HashSet<JsonObject> values = getFacetArray(dataset, displayId, displayTitle);
        for (JsonObject current : values) {
            if (id.equalsIgnoreCase(current.getString(displayId))) {
                Object titleValue = current.getValue(displayTitle);
                return titleValue != null ? titleValue : id;
            }
        }
        return id; // Return the provided id if no matching title is found
    }

    public static String getCatalogTitle(JsonObject catalog) {
        JsonArray catalogLanguages = catalog.getJsonArray("language");

        String catalogLanguageId = "";
        if (!Helper.isNullOrEmpty(catalogLanguages)) {
            JsonObject catalogueLanguage = catalogLanguages.getJsonObject(0);
            if (!Helper.isNullOrEmpty(catalogueLanguage)) {
                catalogLanguageId = catalogueLanguage.getString("id");
            }
        }

        String title = "";
        if (!Helper.isNullOrEmpty(catalogLanguageId)) {
            JsonObject catalogTitle = catalog.getJsonObject(TITLE);
            if (catalogTitle != null) {
                title = catalogTitle.getString(catalogLanguageId.toLowerCase());
            }
        }

        if (Helper.isNullOrEmpty(title)) {
            JsonObject catalogTitle = catalog.getJsonObject(TITLE);
            if (catalogTitle != null) {
                title = catalogTitle.getString("en");
            }
        }

        if (Helper.isNullOrEmpty(title)) {
            title = catalog.getString("id");
        }

        return title;
    }

    public static JsonObject buildAutocompleteSearchResult(List<Hit<JsonData>> searchHits, String queryFilter,
                                                           List<String> queryIncludes, String queryQ) {
        JsonArray autocompleteResults = new JsonArray();
        HashMap<String, List<String>> languageFields = SearchResponseHelper.prepareLanguageFields(queryIncludes);
        searchHits.forEach(hit -> {
            Map<String, Object> sourceAsMap = hit.source().to(LinkedHashMap.class.getGenericSuperclass());
            JsonObject hitResult = new JsonObject(sourceAsMap);

            languageFields.keySet().forEach(key ->
                    SearchResponseHelper.handleLanguageFields(hitResult, key, languageFields.get(key)));

            autocompleteResults.add(hitResult);
        });

        JsonArray resultsArr = new JsonArray();
        resultsArr.add(filterAutocompleteResult(autocompleteResults, queryQ));

        JsonObject searchResult = new JsonObject();
        searchResult.put(Constants.SEARCH_RESULT_INDEX_FIELD, queryFilter);
        searchResult.put(Constants.SEARCH_RESULT_RESULTS_FIELD, resultsArr);

        return searchResult;
    }

    private static JsonObject filterAutocompleteResult(JsonArray results, String searchTerm) {
        JsonObject filteredResults = new JsonObject();
        JsonArray titles = new JsonArray();
        JsonArray descriptions = new JsonArray();
        JsonArray keywords = new JsonArray();

        for (int i=0; i<results.size(); i++) {
            JsonObject result = results.getJsonObject(i);
            // Filter titles and descriptions
            addResultToList(titles, result, TITLE, searchTerm);
            addResultToList(descriptions, result, DESCRIPTION, searchTerm);
            // Filter keywords
            JsonArray keywordsArr = result.getJsonArray("keywords");
            if (keywordsArr != null) {
                for (int idx=0; idx<keywordsArr.size(); idx++) {
                    String keyword = keywordsArr.getJsonObject(idx).getString("label");
                    if (keyword.toLowerCase().contains(searchTerm.toLowerCase())) {
                        keywords.add(keywordsArr.getJsonObject(idx));
                    }
                }
            }
        }

        // Remove duplicates in titles and descriptions
        filteredResults.put("titles", removeDuplicates(titles, TITLE));
        filteredResults.put("descriptions", removeDuplicates(descriptions, DESCRIPTION));
        // Remove duplicates in keywords
        Map<String, JsonObject> keywordsMap = new HashMap<>();
        keywords.forEach(keyword -> {
            JsonObject keywordObj = (JsonObject) keyword;
            keywordsMap.putIfAbsent(keywordObj.getString("id"), keywordObj);
        });
        filteredResults.put("keywords", new JsonArray(String.valueOf(keywordsMap.values())));

        return filteredResults;
    }

    private static void addResultToList(JsonArray resultsArr, JsonObject result, String field, String searchTerm) {
        JsonObject descObj = result.getJsonObject(field);
        if (descObj == null) return;

        descObj.getMap().keySet().forEach(key -> {
            String desc = descObj.getString(key);
            if (desc.toLowerCase().contains(searchTerm.toLowerCase()))
                resultsArr.add(result);
        });
    }

    private static JsonArray removeDuplicates(JsonArray results, String key) {
        Map<String, JsonObject> map = new HashMap<>();
        results.forEach(result -> {
            JsonObject resultObj = (JsonObject) result;
            JsonObject fieldObject = resultObj.getJsonObject(key);
            map.putIfAbsent(fieldObject.encode(), resultObj);
        });

        // Convert the values of the map back to a JsonArray
        return new JsonArray(String.valueOf(map.values()));
    }

    private static JsonArray addHitResultToResults(List<Hit<JsonData>> searchHits, QueryParams query,
                                              Map<String, Field> fields, JsonArray countDatasets,
                                              JsonArray datasets, Map<String, JsonObject> datasetFacets) {
        JsonArray results = new JsonArray();
        HashMap<String, List<String>> languageFields = prepareLanguageFields(query.getIncludes());

        searchHits.forEach(hit -> {
            JsonObject hitResult;
            Map<String, Object> sourceAsMap = hit.source().to(LinkedHashMap.class.getGenericSuperclass());

            if (Helper.isNullOrEmpty(fields) || !Helper.isNullOrEmpty(query.getIncludes())) {
                hitResult = new JsonObject(sourceAsMap);
            } else {
                hitResult = new JsonObject();
                handleHitResultField(sourceAsMap, fields, hit, hitResult);
            }

            if (query.isShowScore()) {
                double score = hit.score().doubleValue();
                hitResult.put("score", score);
            }

            languageFields.keySet().forEach(key ->
                    handleLanguageFields(hitResult, key, languageFields.get(key)));
            handleHitResultJsonData(hit.fields().get("_ignored"), hitResult);

            if (query.isElasticId()) {
                hitResult.put("_id", hit.id());
            }

            if (hit.index() != null) {
                handleCountDatasets(countDatasets, hit.index(), hitResult);
                handleDatasetsAndHitResult(datasets, query.isFilterDistributions(), datasetFacets,
                        query.getFacets(), hit.index(), hitResult);
            }

            results.add(hitResult);
        });

        return results;
    }

    private static HashMap<String, List<String>> prepareLanguageFields(List<String> includes) {
        HashMap<String, List<String>> languageFields = new HashMap<>();
        languageFields.put(TITLE, new ArrayList<>());
        languageFields.put(DESCRIPTION, new ArrayList<>());
        if (includes != null) {
            for (String include : includes) {
                if (include.contains("title.")) {
                    String[] split = include.split("\\.");
                    if (split.length == 2) {
                        languageFields.get(TITLE).add(include.split("\\.")[1]);
                    }
                } else if (include.contains("description.")) {
                    String[] split = include.split("\\.");
                    if (split.length == 2) {
                        languageFields.get(DESCRIPTION).add(include.split("\\.")[1]);
                    }
                }
            }
        }
        return languageFields;
    }

    private static void handleHitResultField(Map<String, Object> sourceAsMap, Map<String, Field> fields,
                                             Hit<JsonData> hit, JsonObject hitResult) {
        for (String field : fields.keySet()) {
            if (hit.index().startsWith(Constants.DOC_TYPE_DATASET) && field.equals(DISTRIBUTIONS)
                    && sourceAsMap.get(DISTRIBUTIONS) == null) {
                hitResult.put(DISTRIBUTIONS, new JsonArray());
            } else {
                hitResult.put(field, sourceAsMap.get(field));
            }
        }
    }

    private static void handleHitResultJsonData(JsonData doc, JsonObject hitResult) {
        if (doc == null) return;

        final String MODIFIED = "modified";
        final String ISSUED = "issued";
        List<String> docs = doc.to(List.class);
        docs.forEach(value -> {
            if (MODIFIED.equals(value) || ISSUED.equals(value)) {
                String docValue = hitResult.getString(value);
                if (docValue != null && !docValue.isEmpty() && docValue.charAt(0) == '_') {
                    hitResult.put(value, docValue.substring(1));
                }
            } else {
                hitResult.putNull(value);
            }
        });
    }

    private static void handleCountDatasets(JsonArray countDatasets, String hitIndex, JsonObject hitResult) {
        if (hitIndex.startsWith(Constants.DOC_TYPE_CATALOGUE)) {
            countDatasets.add(hitResult);
        }
    }
    private static void handleDatasetsAndHitResult(JsonArray datasets, boolean isQueryFilterDist, Map<String, JsonObject> datasetFacets,
                                                   HashMap<String, String[]> queryFacets, String hitIndex, JsonObject hitResult) {
        if (!hitIndex.startsWith(Constants.DOC_TYPE_DATASET)) {
            return;
        }

        datasets.add(hitResult);
        if (!isQueryFilterDist) {
            return;
        }

        for (String facet : datasetFacets.keySet()) {
            String[] facetSplit = facet.split("\\.");
            if (facetSplit.length != 2 || !facetSplit[0].equals(DISTRIBUTIONS)) {
                return;
            }
            String facetSplitDist = facetSplit[1];
            if (queryFacets.get(facetSplitDist) == null) {
                return;
            }

            JsonArray distributions = hitResult.getJsonArray(DISTRIBUTIONS);
            JsonArray distributionsFiltered = new JsonArray();
            for (Object distribution : distributions) {
                JsonObject distJson = (JsonObject) distribution;
                JsonObject distFacet = distJson.getJsonObject(facetSplitDist);
                if (distFacet != null && (Arrays.asList(queryFacets.get(facetSplitDist))
                        .contains(distFacet.getString("id")))) {
                    distributionsFiltered.add(distJson);
                }
            }
            hitResult.put(DISTRIBUTIONS, distributionsFiltered);
        }
    }

    private static JsonArray processAggregations(Map<String, Aggregate> aggregations, Integer queryAggregationLimit,
                                                 Integer queryAggregationMinCount, List<JsonObject> facetOrder) {
        JsonObject facets = new JsonObject();
        JsonArray facetsOrdered = new JsonArray();

        if ( aggregations.get("global") != null ) {
            GlobalAggregate globalAggregation = aggregations.get("global").global();
            aggregations = globalAggregation.aggregations();
        }

        aggregations.entrySet().forEach(agg -> {
            JsonObject facet = createFacetAggregationResult(agg.getKey(), agg.getValue(),
                    queryAggregationLimit, queryAggregationMinCount);

            facets.put(facet.getString("id"), facet);
        });

        facetOrder.forEach(f -> {
            String facetName = f.getString("name");
            String facetType = f.getString("type");

            if ("nested".equals(facetType)) {
                handleNestedFacet(f, facets, facetsOrdered);
            } else {
                handleNonNestedFacet(facetName, facets, facetsOrdered);
            }
        });

        return facetsOrdered;
    }

    private static void handleNestedFacet(JsonObject f, JsonObject facets, JsonArray facetsOrdered) {
        JsonArray items = new JsonArray();
        JsonObject parentFacet = new JsonObject()
                .put("id", f.getString("name"))
                .put(TITLE, f.getString(TITLE))
                .put(ITEMS, items);
        JsonArray childFacets = f.getJsonArray("facets");
        for (Object obj : childFacets) {
            JsonObject childFacet = (JsonObject) obj;
            String childFacetName = childFacet.getString("name");
            items.add(facets.getJsonObject(childFacetName));
        }
        facetsOrdered.add(parentFacet);
    }

    private static void handleNonNestedFacet(String facetName, JsonObject facets, JsonArray facetsOrdered) {
        if (facets.getJsonObject(facetName) != null) {
            facetsOrdered.add(facets.getJsonObject(facetName));
        }
    }

    private static void addMinToFacet(JsonObject facet, MinAggregate valueMin) {
        facet.put(TITLE, valueMin.meta().get(TITLE).toJson().toString().replace("\"", ""));
        facet.put("min", valueMin);
    }
    private static void addMaxToFacet(JsonObject facet, MaxAggregate valueMax) {
        facet.put(TITLE, valueMax.meta().get(TITLE).toJson().toString().replace("\"", ""));
        facet.put("max", valueMax);
    }
    private static void addRangeToFacet(JsonObject facet, RangeAggregate valueRange) {
        final String FROM = "from";
        final String TO = "to";
        double fromDouble = Double.parseDouble(valueRange.meta().get(FROM).toJson().toString());
        double toDouble = Double.parseDouble(valueRange.meta().get(TO).toJson().toString());

        facet.put(TITLE, valueRange.meta().get(TITLE).toJson().toString().replace("\"", ""));
        valueRange.buckets().array().forEach(bucket ->
            facet.put(COUNT, bucket.docCount()));
        facet.put(FROM, (int) fromDouble);
        facet.put(TO, (int) toDouble);
    }
    private static void addLtermsToFacet(JsonObject facet, LongTermsAggregate valueLterms) {
        facet.put(TITLE, valueLterms.meta().get(TITLE).toJson().toString().replace("\"", ""));
        facet.put(ITEMS, new JsonArray());

        valueLterms.buckets().array().forEach(bucket -> {
            if("1".equals(bucket.key())) {
                JsonObject item = new JsonObject()
                        .put(COUNT, bucket.docCount())
                        .put("id", "true")
                        .put(TITLE, "true");
                facet.getJsonArray(ITEMS).add(item);
            }
            if("0".equals(bucket.key())) {
                JsonObject item = new JsonObject()
                        .put(COUNT, bucket.docCount())
                        .put("id", "false")
                        .put(TITLE, "false");
                facet.getJsonArray(ITEMS).add(item);
            }
        });
    }
    private static void addStermsToFacet(JsonObject facet, StringTermsAggregate valueSterms, Integer queryAggregationLimit, Integer queryAggregationMinCount) {
        facet.put(TITLE, valueSterms.meta().get(TITLE).toJson().toString().replace("\"", ""));
        facet.put(ITEMS, new JsonArray());
        int count = 0;
        for (StringTermsBucket bucket : valueSterms.buckets().array()) {
            String id = bucket.key().stringValue();

            JsonObject item = new JsonObject()
                    .put(COUNT, bucket.docCount())
                    .put("id", id)
                    .put(TITLE, id);

            if ((queryAggregationLimit <= 0 || count < queryAggregationLimit) && bucket.docCount() >= queryAggregationMinCount) {
                facet.getJsonArray(ITEMS).add(item);
                count++;
            }
        }
    }
    private static void addFilterToFacet(JsonObject facet, FilterAggregate valueFilter) {
        facet.put(TITLE, valueFilter.meta().get(TITLE).toJson().toString().replace("\"", ""));
        facet.put(COUNT, valueFilter.docCount());
    }

    private static JsonObject createFacetAggregationResult(String key, Aggregate value,
                                                           Integer queryAggregationLimit, Integer queryAggregationMinCount) {
        JsonObject facet = new JsonObject().put("id", key);

        if (value.isMin()) {
            addMinToFacet(facet, value.min());
        }
        if (value.isMax()) {
            addMaxToFacet(facet, value.max());
        }
        if (value.isRange()) {
            addRangeToFacet(facet, value.range());
        }
        if (value.isLterms()) {
            addLtermsToFacet(facet, value.lterms());
        }
        if (value.isSterms()) {
            addStermsToFacet(facet, value.sterms(), queryAggregationLimit, queryAggregationMinCount);
        }
        if (value.isFilter()) {
            addFilterToFacet(facet, value.filter());
        }

        return facet;
    }

    private static HashSet<JsonObject> getFacetArray(JsonObject dataset, String displayId, String displayTitle) {
        HashSet<JsonObject> result = new HashSet<>();

        dataset.getMap().keySet().forEach(key -> {
            Object value = dataset.getValue(key);

            if (value instanceof JsonArray valueArray) {
                valueArray.forEach(arrayValue -> {
                    if (arrayValue instanceof JsonObject valueObject) {
                        if (valueObject.getMap().containsKey(displayId)
                                && valueObject.getMap().containsKey(displayTitle)) {
                            result.add(valueObject);
                        } else {
                            result.addAll(getFacetArray(valueObject, displayId, displayTitle));
                        }
                    }
                });
            }

            if (value instanceof JsonObject valObj) {
                result.add(valObj);
            }
        });

        return result;
    }

}
