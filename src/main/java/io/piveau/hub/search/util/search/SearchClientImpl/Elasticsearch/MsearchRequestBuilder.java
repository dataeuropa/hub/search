package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.MsearchRequest;
import co.elastic.clients.elasticsearch.core.msearch.MultisearchBody;
import co.elastic.clients.elasticsearch.core.msearch.RequestItem;
import co.elastic.clients.elasticsearch.core.search.TrackHits;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.Field;
import io.piveau.hub.search.util.request.QueryParams;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MsearchRequestBuilder extends RequestBuilder {

    public MsearchRequest buildMultipleSearchRequest(QueryParams query, IndexManager indexManager, Operator operator) {
        List<String> filters = query.getFilters();
        MsearchRequest.Builder mSearchRequestBuilder = new MsearchRequest.Builder();

        List<RequestItem> reqItems = new ArrayList<>();
        filters.forEach(filter -> {
            RequestItem.Builder reqItemBuilder = new RequestItem.Builder();
            buildMsearchRequestHeader(reqItemBuilder, filter, query.getVocabulary());
            buildMsearchRequestBody(reqItemBuilder, filter, query, indexManager, operator);

            reqItems.add(reqItemBuilder.build());
        });

        return mSearchRequestBuilder.searches(reqItems).build();
    }

    public MsearchRequest buildMultipleAggregationRequest(QueryParams query, IndexManager indexManager, Operator operator) {
        List<String> filters = query.getFilters();
        MsearchRequest.Builder mSearchRequestBuilder = new MsearchRequest.Builder();

        List<RequestItem> reqItems = new ArrayList<>();
        filters.forEach(filter -> {
            RequestItem.Builder reqItemBuilder = new RequestItem.Builder();
            buildMsearchRequestHeader(reqItemBuilder, filter, query.getVocabulary());
            buildMsearchRequestBodyAggregation(reqItemBuilder, filter, query, indexManager, operator);

            reqItems.add(reqItemBuilder.build());
        });

        return mSearchRequestBuilder.searches(reqItems).build();
    }

    private static void buildMsearchRequestHeader(RequestItem.Builder reqItemBuilder, String filter,
                                                  List<String> vocabularies) {
        if (Constants.DOC_TYPE_VOCABULARY.equals(filter)) {
            if (Helper.isNullOrEmpty(vocabularies)) {
                reqItemBuilder.header(builder -> builder.index("vocabulary_*"));
            } else {
                vocabularies.forEach(vocabularyId -> reqItemBuilder
                        .header(builder -> builder
                                .index("vocabulary_"+vocabularyId)));
            }
        } else {
            reqItemBuilder.header(builder -> builder.index(Constants.getReadAlias(filter)));
        }
    }

    private static void buildMsearchRequestBody(RequestItem.Builder reqItemBuilder, String filter,
                                                QueryParams query, IndexManager indexManager, Operator operator) {
        Integer maxResultWindow = indexManager.getMaxResultWindow().get(filter);
        Map<String, Field> fields = indexManager.getFields().get(filter);
        List<String> queryIncludes = query.getIncludes();
        List<String> querySort = query.getSort();
        BoolQuery fullQuery = buildFullQuerySearchRequest(indexManager, filter, query, operator);

        MultisearchBody.Builder mSearchBodyBuilder = new MultisearchBody.Builder();
        mSearchBodyBuilder.query(Query.of(qBuilder -> qBuilder
                .bool(fullQuery)));
        mSearchBodyBuilder.trackTotalHits(TrackHits.of(thBuilder -> thBuilder
                .enabled(true)));

        applyRange(query.getFrom(), query.getSize(), maxResultWindow, mSearchBodyBuilder::from, mSearchBodyBuilder::size);
        if ( !Helper.isNullOrEmpty(querySort) && fields != null )
            applySorting(mSearchBodyBuilder, querySort, fields, MultisearchBody.Builder::sort);
        if ( !Helper.isNullOrEmpty(queryIncludes) ) {
            applySourceConfig(mSearchBodyBuilder, queryIncludes, MultisearchBody.Builder::source);
        }

        reqItemBuilder.body(mSearchBodyBuilder.build());
    }

    private static void buildMsearchRequestBodyAggregation(RequestItem.Builder reqItemBuilder, String filter,
                                                           QueryParams query, IndexManager indexManager, Operator operator) {
        Integer maxAggSize = indexManager.getMaxAggSize().get(filter);
        Map<String, JsonObject> facets = indexManager.getFacets().get(filter);
        BoolQuery fullQuery = buildFullQueryAggregationRequest(indexManager,
                filter, facets, query, operator);

        MultisearchBody.Builder mSearchBodyBuilder = new MultisearchBody.Builder();
        mSearchBodyBuilder.query(Query.of(qBuilder -> qBuilder.bool(fullQuery)))
                .size(0);

        applyAggregation(mSearchBodyBuilder, query.isAggregationAllFields(), query.getAggregationFields(), maxAggSize,
                facets, MultisearchBody.Builder::aggregations);

        reqItemBuilder.body(mSearchBodyBuilder.build());
    }

}
