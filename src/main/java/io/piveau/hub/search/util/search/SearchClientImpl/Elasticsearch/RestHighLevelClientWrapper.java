package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch.ElasticsearchAsyncClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.*;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import co.elastic.clients.elasticsearch._types.query_dsl.TermQuery;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.msearch.MultiSearchItem;
import co.elastic.clients.elasticsearch.core.msearch.MultiSearchResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.ResponseBody;
import co.elastic.clients.elasticsearch.core.search.SourceConfigParam;
import co.elastic.clients.elasticsearch.indices.*;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.elasticsearch.indices.update_aliases.Action;
import co.elastic.clients.elasticsearch.indices.update_aliases.AddAction;
import co.elastic.clients.elasticsearch.indices.update_aliases.RemoveAction;
import co.elastic.clients.json.JsonData;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.hash.HashHelper;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.json.JsonHelper;
import io.piveau.hub.search.util.request.Field;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.RestHighLevelClientBuilder;
import org.elasticsearch.client.indexlifecycle.LifecyclePolicy;
import org.elasticsearch.client.indexlifecycle.PutLifecyclePolicyRequest;
import org.elasticsearch.client.indices.PutComposableIndexTemplateRequest;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.cluster.metadata.ComposableIndexTemplate;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

import static io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch.IndexHelper.processPayloadLanguageFields;
import static io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch.IndexHelper.processResultLanguageFields;

/**
 * The implementation of the Elasticsearch client
 */

public class RestHighLevelClientWrapper implements SearchClient {

    private final Logger log = LoggerFactory.getLogger(getClass());

    RestHighLevelClient clientDEPRECATED;
    ElasticsearchClient esClient;
    ElasticsearchAsyncClient esAsyncClient;

    // Circuit breaker
    CircuitBreaker breaker;

    // For id hashing
    String hashingAlgorithm;

    // IndexManager
    private final IndexManager indexManager;

    private final Operator operator;
    // vertx context
    private final Vertx vertx;

    public RestHighLevelClientWrapper(Vertx vertx, JsonObject config, IndexManager indexManager) {
        this.vertx = vertx;
        this.indexManager = indexManager;

        String host = config.getString("host", "localhost");
        Integer port = config.getInteger("port", 9200);
        String scheme = config.getString("scheme", "http");
        String user = config.getString("user", "elastic");
        String password = config.getString("password", "");
        Integer circuitBreakerTries = config.getInteger("circuitBreakerTries", 10);
        this.hashingAlgorithm = config.getString("hashingAlgorithm", "SHA3-256");
        this.operator = config.getString("operator", "OR").equals("OR") ? Operator.Or : Operator.And;

        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user, password));

        RestClient httpClient = RestClient.builder(new HttpHost(host, port, scheme))
                .setRequestConfigCallback(requestConfigBuilder ->
                        requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(60000))
                .setHttpClientConfigCallback(
                        httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
                ).build();

        this.clientDEPRECATED = new RestHighLevelClientBuilder(httpClient)
                // Migrate to Java API
                .setApiCompatibilityMode(true)
                .build();
        // Create the Java API Client with the same low-level client
        ElasticsearchTransport transport = new RestClientTransport(httpClient, new JacksonJsonpMapper());
        this.esClient = new ElasticsearchClient(transport);
        this.esAsyncClient = new ElasticsearchAsyncClient(transport);

        this.breaker = CircuitBreaker.create("elasticsearch-breaker", vertx,
                new CircuitBreakerOptions()
                        .setMaxRetries(circuitBreakerTries)
                        .setTimeout(-1)
        ).retryPolicy((t, c) -> {
            log.debug("Retry {}", c);
            return c * 1000L;
        });
    }

    @Override
    public Future<String> postDocument(String type, boolean hashId, JsonObject payload) {
        Promise<String> promise = Promise.promise();

        String documentId = UUID.randomUUID().toString();

        String originId = documentId;

        documentId = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;
        String finalDocumentId = documentId;

        payload.put("id", originId);
        processPayloadLanguageFields(payload);

        String alias = Constants.getWriteAlias(type);

        co.elastic.clients.elasticsearch.core.IndexRequest<JsonData> indexRequest = co.elastic.clients.elasticsearch.core.IndexRequest.of(objBuilder -> objBuilder
                .index(alias)
                .id(finalDocumentId)
                .opType(OpType.Create)
                .withJson(new StringReader(payload.encode())));
        breaker.execute(breakerPromise ->
                esAsyncClient.index(indexRequest)
                        .whenComplete((indexResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(originId, (Exception) exception, breakerPromise, promise);
                            } else {
                                promise.complete(originId);
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });

        return promise.future();
    }

    @Override
    public Future<Void> patchDocument(String type, String documentId, boolean hashId, JsonObject payload) {
        Promise<Void> promise = Promise.promise();

        String originId = documentId;

        documentId = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;

        payload.put("id", originId);
        processPayloadLanguageFields(payload);

        String alias = Constants.getWriteAlias(type);

        String finalDocumentId = documentId;
        UpdateRequest<JsonData, JsonData> updateRequest = UpdateRequest.of(objBuilder -> objBuilder
                .index(alias)
                .id(finalDocumentId)
                .doc(JsonData.fromJson(payload.toString())));
        breaker.execute(breakerPromise ->
                esAsyncClient.update(updateRequest, JsonData.class)
                        .whenComplete((updateResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(originId, (Exception) exception, breakerPromise, promise);
                            } else {
                                promise.complete();
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });
        return promise.future();
    }

    @Override
    public Future<Integer> putDocument(String type, String documentId, boolean hashId,
                                       JsonObject payload) {
        Promise<Integer> promise = Promise.promise();

        String originId = documentId;

        documentId = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;

        payload.put("id", originId);
        if (!type.startsWith("resource_")) {
            processPayloadLanguageFields(payload);
        }

        String alias = Constants.getWriteAlias(type);

        String finalDocumentId = documentId;
        co.elastic.clients.elasticsearch.core.IndexRequest<JsonData> indexRequest = co.elastic.clients.elasticsearch.core.IndexRequest.of(objBuilder -> objBuilder
                .index(alias)
                .id(finalDocumentId)
                .withJson(new StringReader(payload.encode())));
        breaker.execute(breakerPromise ->
                esAsyncClient.index(indexRequest)
                        .whenComplete((indexResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(originId, (Exception) exception, breakerPromise, promise);
                            } else {
                                int respStatus = 201;
                                if (indexResponse.result().toString().equals("Updated")) {
                                    respStatus = 200;
                                }
                                promise.complete(respStatus);
                                breakerPromise.complete();
                            }
                        })

        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });

        return promise.future();
    }

    @Override
    public Future<String> indexDocument(String type, String documentId, JsonData docSource) {
        String finalDocumentId = HashHelper.hashId(hashingAlgorithm, documentId);
        co.elastic.clients.elasticsearch.core.IndexRequest<JsonData> indexRequest = co.elastic.clients.elasticsearch.core.IndexRequest.of(r -> r
                .index(Constants.getWriteAlias(type))
                .id(finalDocumentId)
                .document(docSource));

        return Future.future(promise -> esAsyncClient.index(indexRequest)
                .whenComplete((indexResponse, exception) -> {
                    if (exception != null) {
                        promise.fail(exception);
                    } else {
                        log.info("{} {} is indexed", type, documentId);
                        promise.complete(documentId);
                    }
                })
        );
    }

    @Override
    public Future<JsonObject> getDocument(String type, String alias, String documentId, boolean hashId) {
        Promise<JsonObject> promise = Promise.promise();

        String originId = documentId;
        documentId = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;
        String finalDocumentId = documentId;

        GetRequest getRequestJavaApi = GetRequest.of(r -> r
                .index(alias)
                .id(finalDocumentId)
                .source(SourceConfigParam.of(scpBuilder -> scpBuilder.fetch(true)))
        );
        breaker.execute(breakerPromise ->
                esAsyncClient.get(getRequestJavaApi, JsonData.class)
                        .whenComplete((getResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(originId, (Exception) exception, breakerPromise, promise);
                            } else {
                                if (getResponse.found()) {
                                    JsonObject response = getResponseJavaApiToJson(getResponse, indexManager.getFields().get(type));
                                    if (!type.startsWith("resource_")) {
                                        processResultLanguageFields(response);
                                    }
                                    promise.complete(response);
                                } else {
                                    promise.fail(new ServiceException(404, "Document not found"));
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onFailure(breakerResult ->
                promise.fail(new ServiceException(500, breakerResult.getMessage()))
        );

        return promise.future();
    }

    @Override
    public Future<co.elastic.clients.elasticsearch.core.GetResponse<JsonData>> getDocumentById(String index, String documentId, boolean isHashingRequired) {
        String finalDocumentId = isHashingRequired ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;
        GetRequest getRequest = GetRequest.of(r -> r
                .index(index)
                .id(finalDocumentId));

        return Future.future(promise -> esAsyncClient.get(getRequest, JsonData.class)
                .whenComplete((getResponse, exception) -> {
                    if (exception != null) {
                        log.error(exception.getMessage());
                        promise.fail(exception);
                    } else {
                        promise.complete(getResponse);
                    }
                }));
    }

    @Override
    public Future<JsonObject> getDocumentByTermQuery(String type, String alias, String field, String value) {
        Promise<JsonObject> promise = Promise.promise();
        Query termQuery = Query.of(qBuilder -> qBuilder
                .term(TermQuery.of(tqBuilder -> tqBuilder
                        .field(field)
                        .value(value))));
        SearchRequest searchRequest = SearchRequest.of(srBuilder -> srBuilder
                .index(alias)
                .query(termQuery));

        breaker.execute(breakerPromise ->
                esAsyncClient.search(searchRequest, JsonData.class)
                        .whenComplete((searchResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                co.elastic.clients.elasticsearch.core.search.TotalHits totalHits = searchResponse.hits().total();
                                if (totalHits != null && totalHits.value() == 1) {
                                    Hit<JsonData> searchHit = searchResponse.hits().hits().get(0);
                                    jakarta.json.JsonObject sourceAsJson = searchHit.source().toJson().asJsonObject();
                                    promise.complete(new JsonObject(sourceAsJson.toString()));
                                } else {
                                    promise.fail(new ServiceException(404, "not found"));
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });

        return promise.future();
    }

    @Override
    public Future<Void> deleteDocument(String type, String documentId, boolean hashId) {
        Promise<Void> promise = Promise.promise();

        String originId = documentId;

        documentId = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;
        String finalDocumentId = documentId;

        String alias = Constants.getWriteAlias(type);

        DeleteRequest deleteRequest = DeleteRequest.of(reqBuilder -> reqBuilder
                .index(alias)
                .id(finalDocumentId));

        breaker.execute(breakerPromise ->
                esAsyncClient.delete(deleteRequest)
                        .whenComplete((response, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(originId, (Exception) exception, breakerPromise, promise);
                            } else {
                                if (response.result() == Result.NotFound) {
                                    promise.fail(new ServiceException(404, "not found"));
                                    log.error("Failed to delete {} {}. ERROR: {}", type, originId, Result.NotFound);
                                } else {
                                    promise.complete();
                                    log.info("Deleted {} {}", type, originId);
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });

        return promise.future();
    }

    @Override
    public Future<Long> countDocuments(String type, String idField, String documentId) {
        Promise<Long> promise = Promise.promise();

        Query termQuery = QueryBuilders.term()
                .field(idField)
                .value(documentId)
                .build()._toQuery();
        String alias = Constants.getReadAlias(type);

        CountRequest countRequest = CountRequest.of(objBuilder -> objBuilder
                .index(alias).query(termQuery));
        breaker.execute(breakerPromise ->
                esAsyncClient.count(countRequest)
                        .whenComplete((countResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticException(documentId, (Exception) exception, breakerPromise, promise);
                            } else {
                                promise.complete(countResponse.count());
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });

        return promise.future();
    }

    public Future<Void> updateVocabularyByQuery(String vocabulary, JsonArray vocab, List<String> types) {
        Promise<Void> promise = Promise.promise();

        JsonObject vocabularyConfig = indexManager.getVocabulary().get(vocabulary);
        if (vocabularyConfig == null) {
            promise.complete();
            return promise.future();
        }

        UpdateByQueryRequest.Builder updateByQueryRequest = new UpdateByQueryRequest.Builder();
        updateByQueryRequest.index(types.stream().map(Constants::getWriteAlias).toList());
        updateByQueryRequest.conflicts(Conflicts.Proceed);

        String replacementKey = vocabularyConfig.getString("replacementKey", "resource");
        Map<String, Object> map = new HashMap<>();
        for (Object obj : vocab) {
            JsonObject vocable = (JsonObject) obj;
            map.putIfAbsent(vocable.getString(replacementKey), JsonHelper.convertJsonObjectToMap(vocable));
        }
        JsonArray fields = vocabularyConfig.getJsonArray("fields");

        Map<String, JsonData> params = new HashMap<>();
        params.putIfAbsent("vocab", JsonData.of(map));
        params.putIfAbsent("fields", JsonData.of(fields.getList()));

        JsonArray replacements = vocabularyConfig.getJsonArray("replacements");
        String script = ScriptBuilder.buildScript(replacementKey, replacements);

        if (!script.isEmpty()) {
            InlineScript inlineScript = InlineScript.of(is -> is
                    .lang("painless")
                    .source(script)
                    .params(params)
            );
            updateByQueryRequest.script(Script.of(s -> s.inline(inlineScript)));
            breaker.execute(breakerPromise ->
                    esAsyncClient.updateByQuery(updateByQueryRequest.build())
                            .whenComplete((updateByQueryResponse, exception) -> {
                                if (exception != null) {
                                    ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                } else {
                                    log.debug("UpdateByQuery vocab: {}. For updated all datasets.", vocabulary);
                                    promise.complete();
                                    breakerPromise.complete();
                                }
                            })
            ).onComplete(breakerResult -> {
                if (breakerResult.failed()) {
                    promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
                }
            });
        }

        return promise.future();
    }

    private void setMapForUpdateByQuery(Map<String, Object> map, JsonObject payload) {
        for (Map.Entry<String, Object> entry : payload.getMap().entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (value.getClass().equals(JsonArray.class)) {
                List<Object> list = new ArrayList<>();
                for (Object obj : ((JsonArray) value)) {
                    if (obj.getClass().equals(JsonArray.class)) {
                        JsonArray payloadNo_lang = ((JsonArray) obj).copy();
                        payloadNo_lang.forEach(elem -> ((JsonObject) elem).remove("_lang"));
                        list.add(payloadNo_lang.getList());
                    } else if (obj.getClass().equals(JsonObject.class)) {
                        JsonObject payloadNo_lang = ((JsonObject) obj).copy();
                        payloadNo_lang.remove("_lang");
                        list.add(payloadNo_lang.getMap());
                    } else {
                        list.add(obj);
                    }
                }
                map.putIfAbsent(key, list);
            } else if (value.getClass().equals(JsonObject.class)) {
                JsonObject payloadNo_lang = ((JsonObject) value).copy();
                payloadNo_lang.remove("_lang");
                map.putIfAbsent(key, payloadNo_lang.getMap());
            } else {
                map.putIfAbsent(key, value);
            }
        }
    }

    private void setScriptForUpdateByQueryGlobalReplacement(StringBuilder script, JsonObject payload, List<String> globalReplacements,
                                                            boolean replaceAll, Map<String, JsonData> params) {
        for (String replacementKey : globalReplacements) {
            if (payload.containsKey(replacementKey)) {
                script.append("ctx._source.")
                        .append(replacementKey)
                        .append(" = params.")
                        .append(replacementKey)
                        .append(";");
            } else if (replaceAll) {
                script.append("ctx._source.remove('")
                        .append(replacementKey)
                        .append("');");
            }

            if (payload.containsKey(replacementKey)) {
                if (payload.getValue(replacementKey) instanceof JsonObject) {
                    params.putIfAbsent(replacementKey, JsonData.of(payload.getJsonObject(replacementKey).getMap()));
                } else if (payload.getValue(replacementKey) instanceof Boolean) {
                    params.putIfAbsent(replacementKey, JsonData.of(payload.getBoolean(replacementKey)));
                }
            }
        }
    }

    private void setScriptForUpdateByQueryFieldReplacement(StringBuilder script, JsonObject payload, List<String> fieldReplacements,
                                                           boolean replaceAll, Map<String, JsonData> params, String field, Map<String, Object> map) {
        for (String replacementKey : fieldReplacements) {
            if (payload.containsKey(replacementKey)) {
                script.append("ctx._source.")
                        .append(field)
                        .append(".")
                        .append(replacementKey)
                        .append(" = params.")
                        .append(field)
                        .append(".")
                        .append(replacementKey)
                        .append(";");
            } else if (replaceAll) {
                script.append("ctx._source.")
                        .append(field)
                        .append(".remove('")
                        .append(replacementKey)
                        .append("');");
            }
        }
        params.putIfAbsent(field, JsonData.of(map));
    }

    @Override
    public Future<Void> updateByQuery(String type, String idField, String documentId, String field,
                                      List<String> globalReplacements, List<String> fieldReplacements,
                                      JsonObject payload, boolean replaceAll) {
        Promise<Void> promise = Promise.promise();

        String alias = Constants.getWriteAlias(type);

        UpdateByQueryRequest.Builder updateByQueryRequest = new UpdateByQueryRequest.Builder();
        updateByQueryRequest.index(alias);
        updateByQueryRequest.conflicts(Conflicts.Proceed);
        updateByQueryRequest.query(TermQuery.of(tq -> tq
                        .field(idField)
                        .value(documentId))
                ._toQuery());

        Map<String, Object> map = new HashMap<>();
        setMapForUpdateByQuery(map, payload);

        StringBuilder script = new StringBuilder();
        Map<String, JsonData> params = new HashMap<>();
        if (globalReplacements != null && !globalReplacements.isEmpty()) {
            setScriptForUpdateByQueryGlobalReplacement(script, payload, globalReplacements, replaceAll, params);
        }

        if (fieldReplacements != null && !fieldReplacements.isEmpty()) {
            setScriptForUpdateByQueryFieldReplacement(script, payload, fieldReplacements, replaceAll, params, field, map);
        }

        if (!script.toString().isEmpty()) {
            InlineScript inlineScript = InlineScript.of(is -> is
                    .lang("painless")
                    .source(script.toString())
                    .params(params)
            );
            updateByQueryRequest.script(Script.of(s -> s.inline(inlineScript)));
            breaker.execute(breakerPromise ->
                    esAsyncClient.updateByQuery(updateByQueryRequest.build())
                            .whenComplete((updateByQueryResponse, exception) -> {
                                if (exception != null) {
                                    ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                } else {
                                    log.debug("Update {} in {}: {}.", field, type, documentId);
                                    promise.complete();
                                    breakerPromise.complete();
                                }
                            })
            ).onComplete(breakerResult -> {
                if (breakerResult.failed()) {
                    log.error("Update catalogue: {}", breakerResult.cause().getMessage());
                    promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
                }
            });
        }
        return promise.future();
    }

    @Override
    public Future<Void> deleteByQuery(String type, String idField, String documentId) {
        Promise<Void> promise = Promise.promise();

        String alias = Constants.getWriteAlias(type);
        DeleteByQueryRequest deleteByQueryRequest = DeleteByQueryRequest.of(reqBuilder -> reqBuilder
                .index(alias)
                .conflicts(Conflicts.Proceed)
                .query(Query.of(qBuilder -> qBuilder
                        .term(tqBuilder -> tqBuilder
                                .field(idField)
                                .value(documentId))
                ))
        );
        breaker.execute(breakerPromise ->
                esAsyncClient.deleteByQuery(deleteByQueryRequest)
                        .whenComplete((response, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                log.debug("DeleteByQuery: {} deleted all documents inside alias {}.", documentId, alias);
                                promise.complete();
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });
        return promise.future();
    }

    @Override
    public Future<JsonArray> putDocumentsBulk(String type, JsonArray payload, boolean hashId) {
        return putDocumentsBulk(type, "", List.of(), payload, hashId);
    }

    @Override
    public Future<JsonArray> putDocumentsBulk(String type, String revision, List<String> restoreFields,
                                              JsonArray payload, boolean hashId) {
        MultiGetRequest multiGetRequest = new MultiGetRequest();
        HashMap<String, JsonObject> updates = new HashMap<>();

        payload.stream()
                .map(JsonObject.class::cast)
                .forEach(document -> {
                    String documentId = document.getString("id");
                    String idHash = hashId ? HashHelper.hashId(hashingAlgorithm, documentId) : documentId;

                    processPayloadLanguageFields(document);

                    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, null, null);
                    MultiGetRequest.Item multiGetRequestItem = new MultiGetRequest.Item(Constants.getReadAlias(type), idHash)
                            .fetchSourceContext(fetchSourceContext);

                    multiGetRequest.add(multiGetRequestItem);
                    updates.put(documentId, document);
                });

        return Future.<MultiGetResponse>future(promise -> clientDEPRECATED.mgetAsync(multiGetRequest, RequestOptions.DEFAULT, new ActionListener<>() {
                    @Override
                    public void onResponse(MultiGetResponse response) {
                        promise.complete(response);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        promise.fail(e);
                    }
                }))
                .compose(response -> {
                    BulkRequest bulkRequest = new BulkRequest();

                    Map<String, MultiGetItemResponse> responses = Arrays.asList(response.getResponses())
                            .stream()
                            .collect(Collectors.toMap(MultiGetItemResponse::getId, itemResponse -> itemResponse));

                    updates.forEach((id, document) -> {
                        String idHash = hashId ? HashHelper.hashId(hashingAlgorithm, id) : id;
                        if (!responses.get(idHash).isFailed() && responses.get(idHash).getResponse().isExists()) {
                            JsonObject oldDocument = getResponseToJson(responses.get(idHash).getResponse(), indexManager.getFields().get(type));
                            if (!revision.isBlank()) {
                                IndexRequest indexRequest = new IndexRequest(Constants.getWriteAlias(revision))
                                        .id(idHash)
                                        .source(oldDocument.encode(), XContentType.JSON);
                                bulkRequest.add(indexRequest);
                            }

                            for (String field : restoreFields) {
                                if (document.getJsonObject(field) == null ||
                                        document.getJsonObject(field).isEmpty()) {
                                    document.put(field, oldDocument.getJsonObject(field));
                                }
                            }
                        }
                        IndexRequest indexRequest = new IndexRequest(Constants.getWriteAlias(type))
                                .id(idHash)
                                .source(document.encode(), XContentType.JSON);
                        bulkRequest.add(indexRequest);
                    });
                    return sendBulkRequest(bulkRequest, List.copyOf(updates.keySet()), hashId);
                });
    }

    public Future<Set<String>> getAliases(String type) {
        Promise<Set<String>> promise = Promise.promise();
        GetAliasRequest getAliasRequest = GetAliasRequest.of(objBuilder -> objBuilder);
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().getAlias(getAliasRequest)
                        .whenComplete((getAliasResponse, exception) -> {
                            if (exception != null) {
                                log.error("Get aliases not succeeded");
                                promise.fail(new ServiceException(500, "Get aliases not succeeded"));
                            } else {
                                Set<String> dsrSet = new HashSet<>();
                                getAliasResponse.result().keySet().forEach(index -> {
                                    if (index.startsWith(type)) dsrSet.add(index);
                                });
                                promise.complete(dsrSet);
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(new ServiceException(500, breakerResult.cause().getMessage()));
            }
        });
        return promise.future();
    }

    public Future<Object> searchFacetTitle(QueryParams query, String itemId, String facetId, boolean fromIndex) {
        String filter = query.getFilter();
        JsonObject facetJson = indexManager.getFacets().get(filter).get(facetId);

        String displayId = facetJson.getString("display_id", "id");
        String displayTitle = facetJson.getString("display_title", "label");

        SearchRequest searchRequest = SearchRequestBuilder
                .buildSearchRequest(query, indexManager, operator);

        return breaker.execute(breakerPromise -> esAsyncClient
                        .search(searchRequest, JsonData.class)
                        .whenComplete((searchResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleException((Exception) exception, breakerPromise);
                            } else {
                                JsonArray response = SearchResponseHelper.simpleProcessSearchResult(searchResponse);
                                if (!response.isEmpty()) {
                                    Object facetTitle = fromIndex ?
                                            response.getJsonObject(0).getValue(query.getIncludes().get(0)) :
                                            SearchResponseHelper.getFacetTitle(response.getJsonObject(0), itemId, displayId, displayTitle);
                                    breakerPromise.complete(facetTitle);
                                } else {
                                    breakerPromise.fail(new ServiceException(404, "Title " + itemId + " not found"));
                                }
                            }
                        }))
                .recover(throwable -> Future
                        .failedFuture(new ServiceException(500, throwable.getMessage()))
                );
    }

    @Override
    public Future<JsonArray> listIds(QueryParams query, boolean subdivided, boolean onlyIds) {
        Promise<JsonArray> promise = Promise.promise();

        JsonArray ids = new JsonArray();
        SearchRequest searchRequest = SearchRequestBuilder.buildSearchRequest(query, indexManager, operator);

        esAsyncClient.search(searchRequest, JsonData.class)
                .whenComplete((searchResponse, exception) -> {
                    if (exception != null) {
                        ExceptionHandler.handleException((Exception) exception, promise);
                    } else {
                        String scrollId = searchResponse.scrollId();
                        JsonArray results = onlyIds ?
                                SearchResponseHelper.simpleProcessSearchResult(searchResponse, "id") :
                                SearchResponseHelper.simpleProcessSearchResult(searchResponse);
                        if (!results.isEmpty()) {
                            if (subdivided) ids.add(results);
                            else ids.addAll(results);
                            scrollIds(scrollId, subdivided, ids, onlyIds, promise);
                        } else {
                            promise.complete(ids);
                        }
                    }
                });

        return promise.future();
    }

    private void search(QueryParams query, List<String> filters, Promise<JsonObject> promise) {
        SearchRequest searchRequest;
        SearchRequest aggregationRequest = null;

        if (filters != null)
            query.setFilter(filters.get(0));

        if (indexManager.getIndexList().contains(query.getFilter())) {
            searchRequest = SearchRequestBuilder.buildSearchRequest(query, indexManager, operator);
            if (query.isAggregation()) {
                aggregationRequest = SearchRequestBuilder.buildAggregationRequest(query, indexManager, operator);
            }
        } else {
            promise.fail(new ServiceException(400, "Search filter unknown"));
            return;
        }

        Promise<SearchResponse<JsonData>> search = doSearch(searchRequest);
        Promise<SearchResponse<JsonData>> agg = doSearch(aggregationRequest);

        Future.all(search.future(), agg.future())
                .onSuccess(ar -> buildResult(search.future().result(), agg.future().result(), query)
                        .onSuccess(result -> promise.complete(ReturnHelper.returnSuccess(200, result)))
                        .onFailure(promise::fail))
                .onFailure(promise::fail);
    }

    private void multiSearch(QueryParams query, List<String> filters, Promise<JsonObject> promise) {
        MsearchRequestBuilder mSearchRequestBuilder = new MsearchRequestBuilder();
        MsearchRequest mSearchRequest;
        MsearchRequest mAggregationRequest = null;
        String unknownFilter = "";

        for (String filter : filters) {
            if (!indexManager.getIndexList().contains(filter) && !filter.startsWith("resource")) {
                unknownFilter = filter;
                break;
            }
        }

        if (Strings.isNullOrEmpty(unknownFilter)) {
            mSearchRequest = mSearchRequestBuilder.buildMultipleSearchRequest(query, indexManager, operator);
            // an autocomplete request does not need an aggregation
            if (query.isAggregation() && !query.isAutocomplete())
                mAggregationRequest = mSearchRequestBuilder.buildMultipleAggregationRequest(query, indexManager, operator);
        } else {
            promise.fail(new ServiceException(400, "Search filter " + unknownFilter + " is unknown"));
            return;
        }

        Promise<MsearchResponse<JsonData>> mSearch = doMultipleSearch(mSearchRequest);
        Promise<MsearchResponse<JsonData>> mAgg = doMultipleSearch(mAggregationRequest);

        Future.all(mSearch.future(), mAgg.future())
                .onSuccess(ar -> {
                    MsearchResponse<JsonData> mAggregationResponse = mAgg.future().result();
                    List<MultiSearchResponseItem<JsonData>> mAggregationResponses = null;

                    if (mAggregationResponse != null)
                        mAggregationResponses = mAggregationResponse.responses();

                    buildMultipleResult(mSearch.future().result().responses(), mAggregationResponses, query)
                            .onSuccess(result -> promise.complete(ReturnHelper.returnSuccess(200, result)))
                            .onFailure(promise::fail);
                })
                .onFailure(promise::fail);
    }

    @Override
    public Future<JsonObject> search(QueryParams query) {
        Promise<JsonObject> promise = Promise.promise();

        List<String> filters = query.getFilters();
        String filterDEPRECATED = query.getFilter();

        if (filters != null && filters.size() > 1) {
            multiSearch(query, filters, promise);
        } else if (filters != null || !Strings.isNullOrEmpty(filterDEPRECATED)) {
            search(query, filters, promise);
        } else {
            SearchRequest searchRequest = SearchRequestBuilder.buildSearchRequest(query, operator);
            doSearch(searchRequest).future()
                    .onSuccess(ar -> buildResult(ar, null, query)
                            .onSuccess(result -> promise.complete(ReturnHelper.returnSuccess(200, result)))
                            .onFailure(promise::fail))
                    .onFailure(promise::fail);
        }

        return promise.future();
    }

    @Override
    public Future<JsonObject> scroll(String scrollId) {
        Promise<JsonObject> promise = Promise.promise();

        ScrollRequest scrollRequest = ScrollRequest.of(sr -> sr
                .scroll(Time.of(time -> time.time("300s")))
                .scrollId(scrollId));
        Promise<co.elastic.clients.elasticsearch.core.ScrollResponse<JsonData>> search = doScroll(scrollRequest);

        QueryParams query = new QueryParams();
        query.setScroll(true);

        search.future().onComplete(ar -> {
            if (ar.succeeded()) {
                buildResult(search.future().result(), null, query)
                        .onSuccess(result -> promise.complete(ReturnHelper.returnSuccess(200, result)))
                        .onFailure(promise::fail);
            } else {
                promise.fail(new ServiceException(500, ar.cause().getMessage()));
            }
        });

        return promise.future();
    }

    @Override
    public Future<Void> ping() {
        return breaker.execute(promise -> {
            try {
                esClient.ping();
                promise.complete();
            } catch (IOException e) {
                promise.fail(e);
            }
        });
    }

    @Override
    public Future<Boolean> indexExists(String index) {
        Promise<Boolean> promise = Promise.promise();
        ExistsRequest existsRequest = ExistsRequest.of(builder -> builder
                .index(index)
                .allowNoIndices(false));
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().exists(existsRequest)
                        .whenComplete((response, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                promise.complete(response.value());
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(breakerResult.cause().getMessage());
            }
        });
        return promise.future();
    }

    @Override
    public Future<JsonArray> getIndices(String index) {
        Promise<JsonArray> promise = Promise.promise();

        GetIndexRequest getIndexRequest = GetIndexRequest.of(builder ->
                builder.index(index).ignoreUnavailable(false)
                        .allowNoIndices(false));
        // expandToOpenIndices? expandToClosedIndices? expandToHiddenIndices?
        breaker.<JsonArray>execute(breakerPromise ->
                esAsyncClient.indices().get(getIndexRequest)
                        .whenComplete((getIndexResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                JsonArray jsonArray = new JsonArray();
                                getIndexResponse.result().forEach((key, value) ->
                                        jsonArray.add(key)
                                );
                                promise.complete(jsonArray);
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                promise.fail(breakerResult.cause().getMessage());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> indexCreate(String index, Integer numberOfShards) {
        Promise<String> promise = Promise.promise();
        prepareIndexCreateRequest(index, numberOfShards).onSuccess(result ->
                breaker.execute(breakerPromise ->
                        esAsyncClient.indices().create(result)
                                .whenComplete((createIndexResponse, exception) -> {
                                    if (exception != null) {
                                        ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                    } else {
                                        if (createIndexResponse.acknowledged()
                                                && createIndexResponse.shardsAcknowledged()) {
                                            promise.complete("The index was successfully created (" + index + ")");
                                        } else {
                                            promise.fail("Failed to create index (" + index + ")");
                                        }
                                        breakerPromise.complete();
                                    }
                                })
                ).onComplete(breakerResult -> {
                    if (breakerResult.failed()) {
                        log.error("Failed index create: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                })
        ).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> indexDelete(String index) {
        Promise<String> promise = Promise.promise();
        DeleteIndexRequest deleteIndexRequest = DeleteIndexRequest.of(builder -> builder.index(index));
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().delete(deleteIndexRequest)
                        .whenComplete((deleteIndexResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                if (deleteIndexResponse.acknowledged()) {
                                    promise.complete("The index was successfully deleted (" + index + ")");
                                } else {
                                    promise.fail("Failed to delete index (" + index + ")");
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                log.error("Failed index delete: {}", breakerResult.cause().getMessage());
                promise.fail(breakerResult.cause());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> setIndexAlias(String oldIndex, String newIndex, String alias) {
        Promise<String> promise = Promise.promise();
        UpdateAliasesRequest.Builder updateAliasesRequest = new UpdateAliasesRequest.Builder();
        if (oldIndex != null && !oldIndex.isEmpty()) {
            RemoveAction removeAction = RemoveAction.of(r -> r
                    .index(oldIndex)
                    .alias(alias));
            updateAliasesRequest.actions(Action.of(a -> a.remove(removeAction)));
        }
        if (newIndex != null && !newIndex.isEmpty()) {
            AddAction addAction = AddAction.of(a -> a
                    .index(newIndex)
                    .alias(alias));
            updateAliasesRequest.actions(Action.of(a -> a.add(addAction)));
        }
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().updateAliases(updateAliasesRequest.build())
                        .whenComplete((updateAliasesResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                if (updateAliasesResponse.acknowledged()) {
                                    promise.complete("Successfully set: " + alias);
                                } else {
                                    promise.fail("Failed to set: " + alias);
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                log.error("Failed set index alias: {}", breakerResult.cause().getMessage());
                promise.fail(breakerResult.cause());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> putIndexTemplate(String index) {
        Promise<String> promise = Promise.promise();

        preparePutIndexTemplateRequest(index).onSuccess(result ->
                breaker.execute(breakerPromise ->
                        clientDEPRECATED.indices().putIndexTemplateAsync(result, RequestOptions.DEFAULT,
                                new ActionListener<>() {
                                    @Override
                                    public void onResponse(AcknowledgedResponse acknowledgedResponse) {
                                        if (acknowledgedResponse.isAcknowledged()) {
                                            promise.complete("The index template was successfully created (" + index + ")");
                                        } else {
                                            promise.fail("Failed to create index template (" + index + ")");
                                        }
                                        breakerPromise.complete();
                                    }

                                    @Override
                                    public void onFailure(Exception e) {
                                        ExceptionHandler.handleElasticException(e, breakerPromise, promise);
                                    }
                                })
                ).onComplete(breakerResult -> {
                    if (breakerResult.failed()) {
                        log.error("Failed put index template: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                })
        ).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> putLifecyclePolicy(String index) {
        Promise<String> promise = Promise.promise();

        preparePutLifecyclePolicyRequest(index).onSuccess(result ->
                breaker.execute(breakerPromise ->
                        clientDEPRECATED.indexLifecycle().putLifecyclePolicyAsync(result, RequestOptions.DEFAULT,
                                new ActionListener<>() {
                                    @Override
                                    public void onResponse(
                                            org.elasticsearch.client.core.AcknowledgedResponse acknowledgedResponse) {
                                        if (acknowledgedResponse.isAcknowledged()) {
                                            promise.fail("The lifecycle policy was successfully created (" + index + ")");
                                        } else {
                                            promise.fail("Failed to create lifecycle policy (" + index + ")");
                                        }
                                        breakerPromise.complete();
                                    }

                                    @Override
                                    public void onFailure(Exception e) {
                                        ExceptionHandler.handleElasticException(e, breakerPromise, promise);
                                    }
                                })
                ).onComplete(breakerResult -> {
                    if (breakerResult.failed()) {
                        log.error("Failed put lifecycle policy: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                })
        ).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> putMapping(String index) {
        Promise<String> promise = Promise.promise();
        preparePutMappingRequest(index).onSuccess(result ->
                breaker.execute(breakerPromise ->
                        clientDEPRECATED.indices().putMappingAsync(result, RequestOptions.DEFAULT,
                                new ActionListener<>() {
                                    @Override
                                    public void onResponse(AcknowledgedResponse putMappingResponse) {
                                        if (putMappingResponse.isAcknowledged()) {
                                            promise.complete("The mapping was successfully added (" + index + ")");
                                        } else {
                                            promise.fail("Failed to put mapping (" + index + ")");
                                        }
                                        breakerPromise.complete();
                                    }

                                    @Override
                                    public void onFailure(Exception e) {
                                        ExceptionHandler.handleElasticException(e, breakerPromise, promise);
                                    }
                                })
                ).onComplete(breakerResult -> {
                    if (breakerResult.failed()) {
                        log.error("Failed put mapping: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                })
        ).onFailure(promise::fail);
        return promise.future();
    }

    @Override
    public Future<String> setMaxResultWindow(String index, Integer maxResultWindow) {
        Promise<String> promise = Promise.promise();

        if (maxResultWindow == null) {
            promise.fail("Max result window missing");
            return promise.future();
        }

        if (maxResultWindow < 10000) {
            promise.fail("Max result window has to be greate than 10000");
            return promise.future();
        }

        IndexSettings indexSettings = IndexSettings.of(settings -> settings.maxResultWindow(maxResultWindow));
        PutIndicesSettingsRequest putIndicesSettingsRequest = PutIndicesSettingsRequest.of(req -> req
                .index(index)
                .settings(indexSettings));
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().putSettings(putIndicesSettingsRequest)
                        .whenComplete((putIndicesSettingsResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                if (putIndicesSettingsResponse.acknowledged()) {
                                    promise.complete("Successfully set max_result_window = " + maxResultWindow);
                                    indexManager.setMaxResultWindow(index, maxResultWindow);
                                } else {
                                    promise.fail("Failed to set max_result_window");
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                log.error("Failed set max result window: {}", breakerResult.cause().getMessage());
                promise.fail(breakerResult.cause());
            }
        });
        return promise.future();
    }

    @Override
    public Future<String> setNumberOfReplicas(String index, Integer numberOfReplicas) {
        Promise<String> promise = Promise.promise();

        if (numberOfReplicas == null) {
            promise.fail("Number of Replicas missing");
            return promise.future();
        }

        if (numberOfReplicas < 0) {
            promise.fail("Number of Replicas must be greater or equal zero");
            return promise.future();
        }

        IndexSettings indexSettings = IndexSettings.of(is -> is.numberOfReplicas(numberOfReplicas.toString()));
        PutIndicesSettingsRequest putIndicesSettingsRequest = PutIndicesSettingsRequest.of(req -> req
                .index(index)
                .settings(indexSettings));
        breaker.execute(breakerPromise ->
                esAsyncClient.indices().putSettings(putIndicesSettingsRequest)
                        .whenComplete((putIndicesSettingsResponse, exception) -> {
                            if (exception != null) {
                                ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                            } else {
                                if (putIndicesSettingsResponse.acknowledged()) {
                                    promise.complete("Successfully set number_of_replicas = " + numberOfReplicas);
                                } else {
                                    promise.fail("Failed to set number_of_replicas");
                                }
                                breakerPromise.complete();
                            }
                        })
        ).onComplete(breakerResult -> {
            if (breakerResult.failed()) {
                log.error("Failed set max result window: {}", breakerResult.cause().getMessage());
                promise.fail(breakerResult.cause());
            }
        });
        return promise.future();
    }

    /* H E L P E R */

    private Future<PutMappingRequest> preparePutMappingRequest(String index) {
        Promise<PutMappingRequest> promise = Promise.promise();

        JsonObject mapping;

        if (index.startsWith(Constants.DOC_TYPE_DATASET)) {
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASET);
        } else if (index.startsWith(Constants.DOC_TYPE_CATALOGUE)) {
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_CATALOGUE);
        } else if (index.startsWith(Constants.DOC_TYPE_DATASERVICE)) {
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASERVICE);
        } else if (index.startsWith(Constants.DOC_TYPE_VOCABULARY + "_")) {
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_VOCABULARY);
        } else if (index.startsWith(Constants.DOC_TYPE_RESOURCE + "_")) {
            mapping = indexManager.getMappings().get(index.substring(0, index.lastIndexOf("_")));
        } else if (index.startsWith(Constants.DOC_TYPE_DATASETREVISIONS + "_")) {
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASETREVISIONS);
        } else {
            log.error("Wrong index name: {}", index);
            promise.fail("Wrong index name: " + index);
            return promise.future();
        }

        PutMappingRequest putMappingRequest = new PutMappingRequest(index);
        putMappingRequest.source(mapping.toString(), XContentType.JSON);
        promise.complete(putMappingRequest);

        return promise.future();
    }

    private Future<PutLifecyclePolicyRequest> preparePutLifecyclePolicyRequest(String index) {
        Promise<PutLifecyclePolicyRequest> promise = Promise.promise();

        String filePath;

        if (index.equals(Constants.DOC_TYPE_DATASET)) {
            filePath = indexManager.getPolicyFilepath().get(Constants.DOC_TYPE_DATASET);
        } else if (index.equals(Constants.DOC_TYPE_CATALOGUE)) {
            filePath = indexManager.getPolicyFilepath().get(Constants.DOC_TYPE_CATALOGUE);
        } else if (index.equals(Constants.DOC_TYPE_DATASERVICE)) {
            filePath = indexManager.getPolicyFilepath().get(Constants.DOC_TYPE_DATASERVICE);
        } else if (index.startsWith(Constants.DOC_TYPE_VOCABULARY + "_")) {
            filePath = indexManager.getPolicyFilepath().get(Constants.DOC_TYPE_VOCABULARY);
        } else if (index.startsWith(Constants.DOC_TYPE_RESOURCE + "_")) {
            filePath = indexManager.getPolicyFilepath().get(index.substring(0, index.lastIndexOf("_")));
        } else if (index.equals("dataset-revisions")) {
            filePath = indexManager.getPolicyFilepath().get("dataset-revisions");
        } else {
            log.error("Wrong index name: {}", index);
            promise.fail("Wrong index name: " + index);
            return promise.future();
        }

        vertx.fileSystem().readFile(filePath, ar -> {
            if (ar.succeeded()) {
                IndexHelper.generatePhaseMap(ar.result().toString()).future().onSuccess(map -> {
                    LifecyclePolicy lifecyclePolicy = new LifecyclePolicy(index + "-policy", map);
                    PutLifecyclePolicyRequest putLifecyclePolicyRequest = new PutLifecyclePolicyRequest(lifecyclePolicy);
                    promise.complete(putLifecyclePolicyRequest);
                }).onFailure(promise::fail);
            } else {
                promise.fail("Failed to read template file: " + ar.cause().getMessage());
            }
        });

        return promise.future();
    }

    private Future<PutComposableIndexTemplateRequest> preparePutIndexTemplateRequest(String index) {
        Promise<PutComposableIndexTemplateRequest> promise = Promise.promise();

        String tFilepath;
        String sFilepath;
        JsonObject mapping;

        if (index.equals("dataset_write")) {
            tFilepath = indexManager.getTemplateFilepath().get(Constants.DOC_TYPE_DATASET);
            sFilepath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_DATASET);
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASET);
        } else if (index.equals("catalogue_write")) {
            tFilepath = indexManager.getTemplateFilepath().get(Constants.DOC_TYPE_CATALOGUE);
            sFilepath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_CATALOGUE);
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_CATALOGUE);
        } else if (index.equals("dataservice_write")) {
            tFilepath = indexManager.getTemplateFilepath().get(Constants.DOC_TYPE_DATASERVICE);
            sFilepath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_DATASERVICE);
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASERVICE);
        } else if (index.startsWith(Constants.DOC_TYPE_VOCABULARY + "_")) {
            tFilepath = indexManager.getTemplateFilepath().get(Constants.DOC_TYPE_VOCABULARY);
            sFilepath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_VOCABULARY);
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_VOCABULARY);
        } else if (index.startsWith(Constants.DOC_TYPE_RESOURCE + "_")) {
            tFilepath = indexManager.getTemplateFilepath().get(index.substring(0, index.lastIndexOf("_")));
            sFilepath = indexManager.getSettingsFilepath().get(index.substring(0, index.lastIndexOf("_")));
            mapping = indexManager.getMappings().get(index.substring(0, index.lastIndexOf("_")));
        } else if (index.equals("dataset-revisions")) {
            tFilepath = indexManager.getTemplateFilepath().get("dataset-revisions");
            sFilepath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_DATASET);
            mapping = indexManager.getMappings().get(Constants.DOC_TYPE_DATASET);
        } else {
            log.error("Wrong index name: {}", index);
            promise.fail("Wrong index name: " + index);
            return promise.future();
        }

        vertx.fileSystem().readFile(sFilepath, readSettingsFileResult -> {
                    if (readSettingsFileResult.succeeded()) {
                        vertx.fileSystem().readFile(tFilepath, readTemplateFileResult -> {
                            if (readTemplateFileResult.succeeded()) {
                                ComposableIndexTemplate composableIndexTemplate =
                                        IndexHelper.generateIndexTemplate(
                                                readTemplateFileResult.result().toString(),
                                                readSettingsFileResult.result().toString(),
                                                mapping.toString()
                                        );
                                PutComposableIndexTemplateRequest putIndexTemplateRequest =
                                        new PutComposableIndexTemplateRequest();
                                putIndexTemplateRequest.name(index + "-index-template");
                                putIndexTemplateRequest.indexTemplate(composableIndexTemplate);

                                promise.complete(putIndexTemplateRequest);
                            } else {
                                promise.fail("Failed to read template file: " +
                                        readTemplateFileResult.cause().getMessage());
                            }
                        });
                    } else {
                        promise.fail("Failed to read settings file: " + readSettingsFileResult.cause().getMessage());
                    }
                }
        );
        return promise.future();
    }

    private Future<CreateIndexRequest> prepareIndexCreateRequest(String index, Integer numberOfShards) {
        Promise<CreateIndexRequest> promise = Promise.promise();

        String filePath;

        if (index.startsWith(Constants.DOC_TYPE_DATASET + "_")) {
            filePath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_DATASET);
        } else if (index.startsWith(Constants.DOC_TYPE_CATALOGUE + "_")) {
            filePath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_CATALOGUE);
        } else if (index.startsWith(Constants.DOC_TYPE_DATASERVICE + "_")) {
            filePath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_DATASERVICE);
        } else if (index.startsWith(Constants.DOC_TYPE_VOCABULARY + "_")) {
            filePath = indexManager.getSettingsFilepath().get(Constants.DOC_TYPE_VOCABULARY);
        } else if (index.startsWith(Constants.DOC_TYPE_RESOURCE + "_")) {
            filePath = indexManager.getSettingsFilepath().get(index.substring(0, index.lastIndexOf("_")));
        } else if (index.startsWith("dataset-revisions_")) {
            filePath = indexManager.getSettingsFilepath().get("dataset-revisions");
        } else {
            log.error("Wrong index name: {}", index);
            promise.fail("Wrong index name: " + index);
            return promise.future();
        }

        vertx.fileSystem().readFile(filePath, ar -> {
            if (ar.succeeded()) {
                JsonObject settings = ar.result().toJsonObject();
                if (numberOfShards != null) {
                    settings.put("number_of_shards", numberOfShards);
                }
                IndexSettings idxSettings = IndexSettings.of(builder -> builder
                        .withJson(new StringReader(settings.encode())));
                CreateIndexRequest createIndexRequest = CreateIndexRequest.of(
                        builder -> builder.index(index)
                                .settings(idxSettings));
                promise.complete(createIndexRequest);
            } else {
                log.error("Failed to read settings file: {}", ar.cause());
                promise.fail("Failed to read settings file: " + ar.cause().getMessage());
            }
        });

        return promise.future();
    }

    private Promise<co.elastic.clients.elasticsearch.core.ScrollResponse<JsonData>> doScroll(ScrollRequest scrollRequest) {
        Promise<co.elastic.clients.elasticsearch.core.ScrollResponse<JsonData>> promise = Promise.promise();
        if (scrollRequest == null) {
            promise.complete(null);
        } else {
            breaker.<co.elastic.clients.elasticsearch.core.ScrollResponse<JsonData>>execute(breakerPromise ->
                    esAsyncClient.scroll(scrollRequest, JsonData.class)
                            .whenComplete((scrollResponse, exception) -> {
                                if (exception != null) {
                                    ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                } else {
                                    breakerPromise.complete(scrollResponse);
                                }
                            })
            ).onComplete(breakerResult -> {
                if (breakerResult.succeeded()) {
                    promise.complete(breakerResult.result());
                } else {
                    log.error("Search: {}", breakerResult.cause().getMessage());
                    promise.fail(breakerResult.cause());
                }
            });
        }
        return promise;
    }

    private Promise<SearchResponse<JsonData>> doSearch(SearchRequest searchRequest) {
        Promise<SearchResponse<JsonData>> promise = Promise.promise();
        if (searchRequest == null) {
            promise.complete(null);
        } else {
            breaker.<SearchResponse<JsonData>>execute(breakerPromise ->
                    esAsyncClient.search(searchRequest, JsonData.class)
                            .whenComplete((searchResponse, exception) -> {
                                if (exception != null) {
                                    ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                } else {
                                    breakerPromise.complete(searchResponse);
                                }
                            })
            ).onComplete(breakerResult -> {
                if (!promise.future().isComplete()) {
                    if (breakerResult.succeeded()) {
                        promise.complete(breakerResult.result());
                    } else {
                        log.error("Search: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                }
            });
        }
        return promise;
    }

    private Promise<MsearchResponse<JsonData>> doMultipleSearch(MsearchRequest mSearchRequest) {
        Promise<MsearchResponse<JsonData>> promise = Promise.promise();

        if (mSearchRequest == null) {
            promise.complete(null);
        } else {
            breaker.<MsearchResponse<JsonData>>execute(breakerPromise ->
                    esAsyncClient.msearch(mSearchRequest, JsonData.class)
                            .whenComplete((mSearchResponse, exception) -> {
                                if (exception != null) {
                                    ExceptionHandler.handleElasticExceptionJavaApi((Exception) exception, breakerPromise, promise);
                                } else {
                                    breakerPromise.complete(mSearchResponse);
                                }
                            })
            ).onComplete(breakerResult -> {
                if (!promise.future().isComplete()) {
                    if (breakerResult.succeeded()) {
                        promise.complete(breakerResult.result());
                    } else {
                        log.error("Search: {}", breakerResult.cause().getMessage());
                        promise.fail(breakerResult.cause());
                    }
                }
            });
        }
        return promise;
    }

    private Future<JsonObject> buildResult(ResponseBody<JsonData> searchResponse,
                                           ResponseBody<JsonData> aggregationResponse,
                                           QueryParams query) {
        Promise<JsonObject> promise = Promise.promise();
        String filter = query.getFilter();

        JsonArray datasets = new JsonArray();
        JsonArray countDatasets = new JsonArray();
        Map<String, Field> fields = new HashMap<>();
        co.elastic.clients.elasticsearch.core.search.TotalHits totalHits = searchResponse.hits().total();

        JsonObject result = new JsonObject();
        if (query.isAutocomplete() && (filter.equalsIgnoreCase(Constants.DOC_TYPE_CATALOGUE) ||
                filter.equalsIgnoreCase(Constants.DOC_TYPE_DATASET))) {
            // set autocomplete result for catalog and dataset
            result = SearchResponseHelper.buildAutocompleteSearchResult(searchResponse.hits().hits(), filter,
                    query.getIncludes(), query.getQ());
            promise.complete(result);
        } else {
            // set result.index
            if (!Helper.isNullOrEmpty(filter)) {
                result.put("index", filter);
            }

            // set result.count
            result.put(Constants.SEARCH_RESULT_COUNT_FIELD, 0);
            if (totalHits != null) {
                result.put(Constants.SEARCH_RESULT_COUNT_FIELD, totalHits.value());
            }

            // set result.scroll
            if (query.isScroll()) {
                result.put("scrollId", searchResponse.scrollId());
            }

            // set result.facets
            if (aggregationResponse != null && !aggregationResponse.aggregations().isEmpty()) {
                JsonArray facets = SearchResponseHelper
                        .processAggregationResult(query.getAggregationLimit(), query.getAggregationMinCount(),
                                aggregationResponse, indexManager.getFacetOrder().get(filter));
                result.put("facets", facets);
            }

            // set result.results
            JsonArray results = SearchResponseHelper.processSearchResult(
                    searchResponse,
                    query,
                    indexManager.getFacets().get(Constants.DOC_TYPE_DATASET),
                    datasets,
                    countDatasets,
                    fields
            );
            result.put("results", results);

            // set the number of datasets a catalogue has
            List<Future<Void>> futureList = countDocuments(countDatasets);
            JsonObject finalResult = result;
            Future.all(futureList).onComplete(ar -> promise.complete(finalResult));
        }

        return promise.future();
    }

    private Future<JsonArray> buildMultipleResult(List<MultiSearchResponseItem<JsonData>> mSearchResponseItems,
                                                  List<MultiSearchResponseItem<JsonData>> mAggregationResponseItems, QueryParams query) {
        Promise<JsonArray> promise = Promise.promise();
        List<String> filters = query.getFilters();

        JsonArray result = new JsonArray();

        if (query.isAutocomplete()) {
            for (int i = 0; i < filters.size(); i++) {
                String filter = filters.get(i);
                MultiSearchItem<JsonData> searchResponse = mSearchResponseItems.get(i).result();
                JsonObject searchResult = SearchResponseHelper.buildAutocompleteSearchResult(searchResponse.hits().hits(),
                        filter, query.getIncludes(), query.getQ());
                result.add(searchResult);
            }
        } else {
            for (int i = 0; i < filters.size(); i++) {
                String filter = filters.get(i);
                MultiSearchResponseItem<JsonData> mSearchRespItem = mSearchResponseItems.get(i);
                JsonObject resultObj = new JsonObject();

                resultObj.put("index", filter);
                resultObj.put(Constants.SEARCH_RESULT_COUNT_FIELD, 0);
                if (mSearchRespItem.isResult()) {
                    resultObj.put(Constants.SEARCH_RESULT_COUNT_FIELD, mSearchRespItem.result().hits().total().value());

                    if (query.isScroll())
                        resultObj.put("scrollId", mSearchRespItem.result().scrollId());

                    if (mAggregationResponseItems != null) {
                        MultiSearchResponseItem<JsonData> mAggregationRespItem = mAggregationResponseItems.get(i);
                        if (mAggregationRespItem.result().aggregations() != null) {
                            JsonArray facets = SearchResponseHelper
                                    .processMultipleAggregationResult(query.getAggregationLimit(), query.getAggregationMinCount(),
                                            mAggregationRespItem, indexManager.getFacetOrder().get(filter));
                            resultObj.put("facets", facets);
                        }
                    }
                }

                JsonArray datasets = new JsonArray();
                JsonArray countDatasets = new JsonArray();
                Map<String, Field> fields = new HashMap<>();

                JsonArray searchResults = SearchResponseHelper.processMsearchResult(
                        mSearchRespItem,
                        query,
                        indexManager.getFacets().get(Constants.DOC_TYPE_DATASET),
                        datasets,
                        countDatasets,
                        fields
                );
                resultObj.put("results", searchResults);

                List<Future<Void>> futureList = countDocuments(countDatasets);
                Future.all(futureList).onComplete(ar -> result.add(resultObj));
            }
        }
        promise.complete(result);

        return promise.future();
    }

    private List<Future<Void>> countDocuments(JsonArray countDatasets) {
        List<Future<Void>> futureList = new ArrayList<>();

        for (Object value : countDatasets) {
            JsonObject hitResult = (JsonObject) value;

            Promise<Void> countPromise = Promise.promise();
            if (hitResult.getString("id") != null) {
                countDocuments("dataset", "catalog.id.raw", hitResult.getString("id")).onComplete(countDatasetsResult -> {
                    hitResult.put(Constants.SEARCH_RESULT_COUNT_FIELD,
                            countDatasetsResult.succeeded() ? countDatasetsResult.result() : null);
                    countPromise.complete();
                });
            } else {
                countPromise.complete();
            }
            futureList.add(countPromise.future());
        }

        return futureList;
    }

    private void scrollIds(String scrollId, boolean subdivided, JsonArray ids, boolean onlyIds,
                           Promise<JsonArray> promise) {
        ScrollRequest scrollRequest = ScrollRequest.of(sr -> sr
                .scrollId(scrollId)
                .scroll(Time.of(time -> time.time("60s"))));

        esAsyncClient.scroll(scrollRequest, JsonData.class)
                .whenComplete((scrollResponse, exception) -> {
                    if (exception != null) {
                        ExceptionHandler.handleException((Exception) exception, promise);
                    } else {
                        JsonArray results = onlyIds ?
                                SearchResponseHelper.simpleProcessSearchResult(scrollResponse, "id") :
                                SearchResponseHelper.simpleProcessSearchResult(scrollResponse);
                        if (!results.isEmpty()) {
                            if (subdivided) ids.add(results);
                            else ids.addAll(results);
                            scrollIds(scrollResponse.scrollId(), subdivided, ids, onlyIds, promise);
                        } else {
                            promise.complete(ids);
                        }
                    }
                });
    }

    private JsonObject getResponseToJson(GetResponse getResponse, Map<String, Field> fields) {
        Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();

        JsonObject result = new JsonObject(sourceAsMap);
        if (fields != null) {
            result = new JsonObject();
            for (String field : fields.keySet()) {
                if (getResponse.getIndex().startsWith(Constants.DOC_TYPE_DATASET) && field.equals("distributions")
                        && sourceAsMap.get("distributions") == null) {
                    result.put("distributions", new JsonArray());
                } else {
                    result.put(field, sourceAsMap.get(field));
                }
            }
        }

        DocumentField doc = getResponse.getFields().get("_ignored");
        if (doc == null) return result;
        for (Object value : doc.getValues()) {
            if (value.equals("modified")) {
                String modified = result.getString("modified");
                if (modified != null && !modified.isEmpty() && modified.charAt(0) == '_') {
                    result.put("modified", modified.substring(1));
                }
            } else if (value.equals("issued")) {
                String issued = result.getString("issued");
                if (issued != null && !issued.isEmpty() && issued.charAt(0) == '_') {
                    result.put("issued", issued.substring(1));
                }
            } else {
                result.remove(value.toString());
            }
        }

        return result;
    }

    private JsonObject getResponseJavaApiToJson(co.elastic.clients.elasticsearch.core.GetResponse<JsonData> getResponse,
                                                Map<String, Field> fields) {
        final String DISTRIBUTIONS = "distributions";
        final String MODIFIED = "modified";
        final String ISSUED = "issued";

        Map<String, Object> sourceAsMap = getResponse.source().to(LinkedHashMap.class.getGenericSuperclass());
        JsonObject result = new JsonObject(sourceAsMap);
        boolean isDocDatasetType = Constants.DOC_TYPE_DATASET.startsWith(getResponse.index());
        if (fields != null) {
            result = new JsonObject();
            for (String field : fields.keySet()) {
                if (isDocDatasetType && DISTRIBUTIONS.equals(field) && sourceAsMap.get(DISTRIBUTIONS) == null) {
                    result.put(DISTRIBUTIONS, new JsonArray());
                } else {
                    result.put(field, sourceAsMap.get(field));
                }
            }
        }

        DocumentField doc = (DocumentField) getResponse.fields().get("_ignored");
        if (doc == null) return result;
        for (Object value : doc.getValues()) {
            if (MODIFIED.equals(value) || ISSUED.equals(value)) {
                String docValue = result.getString(value.toString());
                if (!Strings.isNullOrEmpty(docValue) && docValue.charAt(0) == '_') {
                    result.put(value.toString(), docValue.substring(1));
                }
            } else {
                result.remove(value.toString());
            }
        }

        return result;
    }

    private Future<JsonArray> sendBulkRequest(BulkRequest bulkRequest, List<String> documentIdList, boolean hashId) {
        if (bulkRequest.numberOfActions() == 0) {
            return Future.succeededFuture(new JsonArray());
        }

        long start = System.currentTimeMillis();
        return breaker.<BulkResponse>execute(promise -> clientDEPRECATED.bulkAsync(bulkRequest, RequestOptions.DEFAULT, new ActionListener<>() {
                    @Override
                    public void onResponse(BulkResponse bulkResponse) {
                        promise.complete(bulkResponse);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        // TODO Check what is worth to do a retry.
                        // Maybe better doing it in the retry method from the breaker
                        if (e instanceof ElasticsearchStatusException esx) {
                            promise.fail(new ServiceException(esx.status().getStatus(), esx.getMessage()));
                        } else {
                            promise.fail(new ServiceException(500, e.getMessage()));
                        }
                    }
                }))
                .map(bulkResponse -> {
                    log.debug("Bulk request to elasticsearch: {}", System.currentTimeMillis() - start);

                    Map<String, BulkItemResponse> responseItems = Arrays.asList(bulkResponse.getItems())
                            .stream()
                            .filter(item -> item.getIndex().equals(Constants.getWriteAlias(Constants.DOC_TYPE_DATASET)))
                            .collect(Collectors.toMap(BulkItemResponse::getId, itemResponse -> itemResponse));

                    JsonArray processedResults = new JsonArray();
                    documentIdList.forEach(id -> {
                        String idHash = hashId ? HashHelper.hashId(hashingAlgorithm, id) : id;
                        if (responseItems.containsKey(idHash)) {
                            BulkItemResponse itemResponse = responseItems.get(idHash);
                            JsonObject result = new JsonObject()
                                    .put("id", id)
                                    .put("status", itemResponse.status().getStatus());
                            if (!itemResponse.isFailed()) {
                                log.trace("Bulk success: Index {}. Document {}.", itemResponse.getIndex(),
                                        itemResponse.getId());
                            } else {
                                result.put("message", itemResponse.getFailureMessage());
                                log.error("Bulk failure: Index {}. Document {}. Status {}. Message {}",
                                        itemResponse.getIndex(), itemResponse.getId(),
                                        itemResponse.status().getStatus(),
                                        itemResponse.getFailureMessage());
                            }
                            processedResults.add(result);
                        }
                    });
                    return processedResults;
                })
                .onFailure(cause -> log.error("Bulk request", cause));
    }

}
