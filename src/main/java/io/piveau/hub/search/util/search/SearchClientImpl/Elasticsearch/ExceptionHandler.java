package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import io.piveau.hub.search.Constants;
import io.vertx.core.Promise;
import io.vertx.serviceproxy.ServiceException;
import org.elasticsearch.ElasticsearchStatusException;

public class ExceptionHandler {

    private ExceptionHandler() {
        throw new IllegalStateException("Utility class");
    }

    private static ServiceException handleElasticException(Exception e) {
        if (e.getClass().equals(ElasticsearchStatusException.class)) {
            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
            return new ServiceException(statusException.status().getStatus(), statusException.getMessage());
        } else {
            return new ServiceException(500, e.getMessage());
        }
    }

    private static ServiceException handleElasticExceptionJavaApi(Exception e) {
        if (e.getClass().equals(ElasticsearchException.class)) {
            ElasticsearchException exception = (ElasticsearchException) e;
            return new ServiceException(exception.response().status(), exception.getMessage());
        } else {
            return new ServiceException(500, e.getMessage());
        }
    }

    public static void handleElasticException(String id, Exception e, Promise breakerPromise, Promise anotherPromise) {
        if (e.getClass().equals(ElasticsearchException.class)) {
            ElasticsearchException exception = (ElasticsearchException) e;
            if (Constants.SECURE_WITH_CIRCUIT_BREAKER.contains(exception.response().status())) {
                breakerPromise.fail(ExceptionHandler.handleElasticExceptionJavaApi(e));
            } else if (exception.response().status() == 404) {
                anotherPromise.fail(new ServiceException(404, id + " not found"));
                breakerPromise.complete();
            } else {
                anotherPromise.fail(
                        new ServiceException(exception.response().status(), exception.getMessage()));
                breakerPromise.complete();
            }
        } else {
            anotherPromise.fail(new ServiceException(500, e.getMessage()));
            breakerPromise.complete();
        }
    }

    public static void handleElasticException(Exception e, Promise breakerPromise, Promise anotherPromise) {
        if (e.getClass().equals(ElasticsearchStatusException.class)) {
            ElasticsearchStatusException statusException = (ElasticsearchStatusException) e;
            if (Constants.SECURE_WITH_CIRCUIT_BREAKER.contains(statusException.status().getStatus())) {
                breakerPromise.fail(ExceptionHandler.handleElasticException(e));
            } else {
                anotherPromise.fail(
                        new ServiceException(statusException.status().getStatus(), statusException.getMessage()));
                breakerPromise.complete();
            }
        } else {
            anotherPromise.fail(new ServiceException(500, e.getMessage()));
            breakerPromise.complete();
        }
    }

    public static void handleElasticExceptionJavaApi(Exception e, Promise breakerPromise, Promise anotherPromise) {
        if (e.getClass().equals(ElasticsearchException.class)) {
            ElasticsearchException exception = (ElasticsearchException) e;
            if (Constants.SECURE_WITH_CIRCUIT_BREAKER.contains(exception.response().status())) {
                breakerPromise.fail(ExceptionHandler.handleElasticExceptionJavaApi(e));
            } else {
                anotherPromise.fail(
                        new ServiceException(exception.response().status(), exception.getMessage()));
                breakerPromise.complete();
            }
        } else {
            anotherPromise.fail(new ServiceException(500, e.getMessage()));
            breakerPromise.complete();
        }
    }

    public static void handleException(Exception e, Promise promise) {
        if (e.getClass().equals(ElasticsearchException.class)) {
            ElasticsearchException exception = (ElasticsearchException) e;
            promise.fail(new ServiceException(exception.response().status(), exception.getMessage()));
        } else {
            promise.fail(new ServiceException(500, e.getMessage()));
        }
    }
}
