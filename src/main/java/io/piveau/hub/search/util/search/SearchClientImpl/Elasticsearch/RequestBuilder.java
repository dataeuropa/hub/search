package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch.core.search.SourceConfig;
import co.elastic.clients.elasticsearch.core.search.SourceFilter;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.Field;
import io.piveau.hub.search.util.request.QueryParams;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.IntConsumer;

public class RequestBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(RequestBuilder.class);
    protected static final String RELEVANCE = "relevance";

    @FunctionalInterface
    protected interface TriConsumer<T, U, V> {
        void accept(T t, U u, V v);
    }

    protected static BoolQuery buildFullQuerySearchRequest(IndexManager indexManager, String filter,
                                                           QueryParams query, Operator operator) {
        JsonObject boost = indexManager.getBoost().get(filter);
        JsonObject options = indexManager.getOptions().get(filter);
        Map<String, JsonObject> facets = indexManager.getFacets().get(filter);
        Map<String, JsonObject> searchParams = indexManager.getSearchParams().get(filter);

        return SearchQueryBuilder
                .buildQuery(query, options, operator, boost, facets, searchParams);
    }

    protected static BoolQuery buildFullQueryAggregationRequest(IndexManager indexManager, String filter,
                                                                Map<String, JsonObject> facets, QueryParams query, Operator operator) {
        JsonObject boost = indexManager.getBoost().get(filter);
        JsonObject options = indexManager.getOptions().get(filter);
        Map<String, JsonObject> searchParams = indexManager.getSearchParams().get(filter);

        if (Boolean.TRUE.equals(query.getGlobalAggregation())) {
            return SearchQueryBuilder
                    .buildGlobalQuery(query.getFilters(), query.getSearchParams(),
                            searchParams);
        } else {
            return SearchQueryBuilder
                    .buildQuery(query, options, operator, boost, facets, searchParams);
        }
    }

    protected static SortOptions buildSortOptionsScore(ImmutablePair<String, SortOrder> sortPair) {
        SortOptions.Builder soBuilder = new SortOptions.Builder();
        return soBuilder.score(ssBuilder -> ssBuilder
                .order(sortPair.getRight())
        ).build();
    }

    protected static SortOptions buildSortOptionsField(ImmutablePair<String, SortOrder> sortPair) {
        SortOptions.Builder soBuilder = new SortOptions.Builder();
        return soBuilder.field(fsBuilder -> fsBuilder
                .field(sortPair.getLeft())
                .order(sortPair.getRight())
        ).build();
    }

    protected static void applyRange(Integer queryFrom, Integer querySize, Integer maxResultWindow,
                                     IntConsumer setFrom, IntConsumer setSize) {
        if (queryFrom + querySize > maxResultWindow) {
            LOG.warn("from + size > max_result_window (" + maxResultWindow + ")");

            if (queryFrom > maxResultWindow) {
                LOG.warn("from > max_result_window; set from = 0 and size = 0");
                setFrom.accept(0);
                setSize.accept(0);
            } else {
                LOG.warn("from <= max_result_window; set size = max_result_window - from = {}",
                        maxResultWindow - queryFrom);
                setFrom.accept(queryFrom);
                setSize.accept(maxResultWindow - queryFrom);
            }
        } else {
            setFrom.accept(queryFrom);
            setSize.accept(querySize);
        }
    }

    protected static <T> void applySorting(T builder, List<String> querySort, Map<String, Field> fields,
                                           BiConsumer<T, SortOptions> sortApplier) {
        List<ImmutablePair<String, SortOrder>> sorts = setGenericSorts(querySort, fields);
        if (sorts.isEmpty())
            return;

        sorts.forEach(sortPair -> {
            SortOptions sortOptions = (RELEVANCE.equals(sortPair.getLeft()) ?
                    buildSortOptionsScore(sortPair) : buildSortOptionsField(sortPair));
            sortApplier.accept(builder, sortOptions);
        });
    }

    protected static <T> void applySourceConfig(T builder, List<String> queryIncludes,
                                                BiConsumer<T, SourceConfig> sourceApplier) {
        SourceConfig sourceConfig = setSourceConfigInclude(queryIncludes);
        sourceApplier.accept(builder, sourceConfig);
    }

    protected static <T> void applyAggregation(T builder, boolean isQueryAggregationAllFields,
                                               List<String> queryAggregationFields, Integer maxAggSize,
                                               Map<String, JsonObject> facets, TriConsumer<T, String, Aggregation> builderApplier) {
        facets.keySet().forEach(facetName -> {
            if (Boolean.TRUE.equals(isQueryAggregationAllFields)  || queryAggregationFields.contains(facetName)) {
                JsonObject facetJson = facets.get(facetName);
                String facetTitle = facetJson.getString("title");
                String facetPath = facetJson.getString("path");
                String facetType = facetJson.getString("type");
                if (facetType == null) {
                    Boolean plain = facetJson.getBoolean("plain", false);
                    String facetAggregationType = facetJson.getString("aggregation_term", "id.raw");
                    builderApplier.accept(builder, facetName, SearchQueryBuilder
                            .genTermsAggregation(facetPath, facetTitle, facetAggregationType, maxAggSize, plain));
                } else {
                    if ("nested".equals(facetType)) {
                        facetJson.getJsonArray("facets").forEach(obj -> {
                            JsonObject nestedFacet = (JsonObject) obj;
                            builderApplier.accept(builder, nestedFacet.getString("name"),
                                    fetchAggregation(maxAggSize, nestedFacet));
                        });
                    } else {
                        builderApplier.accept(builder, facetJson.getString("name"),
                                fetchAggregation(maxAggSize, facetJson));
                    }
                }
            }
        });
    }

    protected static List<ImmutablePair<String, SortOrder>> setGenericSorts(List<String> querySort,
                                                                            Map<String, Field> fields) {
        List<ImmutablePair<String, SortOrder>> sorts = new ArrayList<>();
        querySort.forEach(currentSort -> {
            String[] sortSplit = currentSort.split("\\+");

            String sortField = sortSplit[0];
            SortOrder sortOrder = SortOrder.Desc;

            if (sortSplit.length >= 2 && (sortSplit[1].equalsIgnoreCase("asc"))) {
                sortOrder = SortOrder.Asc;
            }

            if (sortField.equalsIgnoreCase(RELEVANCE)) {
                sorts.add(new ImmutablePair<>(RELEVANCE, sortOrder));
            } else {
                String[] path = sortField.split("\\.");
                Field result = checkSortField(fields.get(path[0]), path, 0);
                if (result != null && (result.getType() != null)) {
                    if ("text".equals(result.getType())) {
                        sorts.add(new ImmutablePair<>(sortField + ".raw", sortOrder));
                    } else if ("keyword".equals(result.getType()) || "date".equals(result.getType()) || "integer".equals(result.getType())) {
                        sorts.add(new ImmutablePair<>(sortField, sortOrder));
                    }
                }
            }
        });

        return sorts;
    }

    protected static SourceConfig setSourceConfigInclude(List<String> queryIncludes) {
        List<String> includesRevised = new ArrayList<>();
        queryIncludes.forEach(include -> {
            includesRevised.add(include);
            if (include.contains("title.") && !includesRevised.contains("title._lang")) {
                includesRevised.add("title._lang");
            } else if (include.contains("description.") && !includesRevised.contains("description._lang")) {
                includesRevised.add("description._lang");
            }
        });

        return SourceConfig.of(scBuilder -> scBuilder
                .filter(SourceFilter.of(sfBuilder -> sfBuilder
                        .includes(includesRevised)))
        );
    }

    protected static Aggregation fetchAggregation(Integer maxAggSize, JsonObject facetJson) {
        String facetTitle = facetJson.getString("title");
        String facetPath = facetJson.getString("path");
        String facetType = facetJson.getString("type");

        switch (facetType) {
            case "min" -> {
                return SearchQueryBuilder.genMinAggregation(facetPath, facetTitle);
            }
            case "max" -> {
                return SearchQueryBuilder.genMaxAggregation(facetPath, facetTitle);
            }
            case "range" -> {
                Double from = facetJson.getDouble("from", Double.MIN_VALUE);
                Double to = facetJson.getDouble("to", Double.MAX_VALUE);
                return SearchQueryBuilder.genRangeAggregation(facetPath, facetTitle, from, to);
            }
            case "mustMatch" -> {
                Boolean boolMustMatch = facetJson.getBoolean("match");
                List<FieldValue> valuesMustMatch = new ArrayList<>();
                for (Object value : facetJson.getJsonArray("values").getList()) {
                    valuesMustMatch.add(FieldValue.of(value.toString()));
                }
                return SearchQueryBuilder.genMustMatchAggregation(facetPath, facetTitle, boolMustMatch, valuesMustMatch);
            }
            case "mustNotMatch" -> {
                Boolean boolMustNotMatch = facetJson.getBoolean("match");
                List<FieldValue> valuesMustNotMatch = new ArrayList<>();
                for (Object value : facetJson.getJsonArray("values").getList()) {
                    valuesMustNotMatch.add(FieldValue.of(value.toString()));
                }
                return SearchQueryBuilder.genMustNotMatchAggregation(facetPath, facetTitle, boolMustNotMatch, valuesMustNotMatch);
            }
            default -> {
                Boolean plain = facetJson.getBoolean("plain", false);
                String facetAggregationTerm = facetJson.getString("aggregation_term", "id.raw");
                return SearchQueryBuilder.genTermsAggregation(facetPath, facetTitle, facetAggregationTerm, maxAggSize, plain);
            }
        }
    }

    private static Field checkSortField(Field current, String[] path, int i) {
        if (current == null) return null;

        if (path.length == ++i) {
            return (current.getSubFields() == null) ? current : null;
        } else {
            if (current.getSubFields() != null) {
                for (Field subField : current.getSubFields()) {
                    if (subField.getName().equals(path[i])) {
                        // Recursive call
                        return checkSortField(subField, path, i);
                    }
                }
            }
            return null;
        }
    }
}
