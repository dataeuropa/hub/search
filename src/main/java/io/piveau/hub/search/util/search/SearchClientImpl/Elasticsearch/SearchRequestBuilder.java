package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.*;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.search.TrackHits;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.Field;
import io.piveau.hub.search.util.request.QueryParams;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * A helper to access elasticsearch SearchRequest
 */

public class SearchRequestBuilder extends RequestBuilder{
    private static final Logger LOG = LoggerFactory.getLogger(SearchRequestBuilder.class);

    public static SearchRequest buildSearchRequest(QueryParams query, Operator operator) {
        BoolQuery fullQuery = SearchQueryBuilder.buildQuery(query, new JsonObject(), operator);

        SearchRequest.Builder searchRequestBuilder = new SearchRequest.Builder();
        searchRequestBuilder.query(Query.of(qBuilder -> qBuilder
                .bool(fullQuery)));
        searchRequestBuilder.trackTotalHits(TrackHits.of(thBuilder ->
                thBuilder.enabled(true)));

        applyRange(query.getFrom(), query.getSize(), 10000, searchRequestBuilder::from, searchRequestBuilder::size);
        if (query.getIncludes() != null && !query.getIncludes().isEmpty())
            applySourceConfig(searchRequestBuilder, query.getIncludes(), SearchRequest.Builder::source);

        searchRequestBuilder.index(Constants.getReadAlias("dataset"),
                Constants.getReadAlias("catalogue"),
                Constants.getReadAlias("dataservice"),
                "vocabulary_*",
                "resource_*");

        if (query.isScroll())
            searchRequestBuilder.scroll(Time.of(tBuilder -> tBuilder.time("60s")));

        return searchRequestBuilder.build();
    }

    public static SearchRequest buildSearchRequest(QueryParams query, IndexManager indexManager, Operator operator) {
        String filter = query.getFilter();
        Integer maxResultWindow = indexManager.getMaxResultWindow().get(filter);
        Map<String, Field> fields = indexManager.getFields().get(filter);
        List<String> querySort = query.getSort();
        BoolQuery fullQuery = buildFullQuerySearchRequest(indexManager, filter, query, operator);

        SearchRequest.Builder searchRequestBuilder = createSearchRequest(
                query.getFilter(), query.getVocabulary(), query.getAlias());
        searchRequestBuilder.query(Query.of(qBuilder -> qBuilder
                .bool(fullQuery)));
        searchRequestBuilder.trackTotalHits(TrackHits.of(thBuilder -> thBuilder
                .enabled(true)));

        applyRange(query.getFrom(), query.getSize(), maxResultWindow, searchRequestBuilder::from, searchRequestBuilder::size);
        if ( !Helper.isNullOrEmpty(querySort) && fields != null )
            applySorting(searchRequestBuilder, querySort, fields, SearchRequest.Builder::sort);
        if ( !Helper.isNullOrEmpty(query.getIncludes()) )
            applySourceConfig(searchRequestBuilder, query.getIncludes(), SearchRequest.Builder::source);
        if (query.isScroll())
            searchRequestBuilder.scroll(Time.of(tBuilder -> tBuilder.time("60s")));

        return searchRequestBuilder.build();
    }

    public static SearchRequest buildAggregationRequest(QueryParams query, IndexManager indexManager, Operator operator) {
        String filter = query.getFilter();
        Integer maxAggSize = indexManager.getMaxAggSize().get(filter);
        Map<String, JsonObject> facets = indexManager.getFacets().get(filter);
        BoolQuery fullQuery = buildFullQueryAggregationRequest(indexManager,
                filter, facets, query, operator);

        SearchRequest.Builder searchRequestBuilder = createSearchRequest(
                query.getFilter(), query.getVocabulary(), query.getAlias());
        searchRequestBuilder.query(Query.of(qBuilder -> qBuilder.bool(fullQuery)))
                .size(0);

        applyAggregation(searchRequestBuilder, query.isAggregationAllFields(), query.getAggregationFields(), maxAggSize,
                facets, SearchRequest.Builder::aggregations);

        return searchRequestBuilder.build();
    }

    private static SearchRequest.Builder createSearchRequest(String queryFilter, List<String> queryVocabularies,
                                                             String queryAlias) {
        SearchRequest.Builder searchRequestBuilder = new SearchRequest.Builder();
        if (Constants.DOC_TYPE_VOCABULARY.equals(queryFilter)) {
            if (Helper.isNullOrEmpty(queryVocabularies)) {
                searchRequestBuilder.index("vocabulary_*");
            } else {
                String[] vocabularies = new String[queryVocabularies.size()];
                int i = 0;
                queryVocabularies.forEach(vocabularyId ->
                        vocabularies[i] = "vocabulary_" + vocabularyId);
                searchRequestBuilder.index(Arrays.asList(vocabularies));
            }
        } else {
            if (Helper.isNullOrEmpty(queryAlias)) {
                searchRequestBuilder.index(Arrays.asList(Constants.getReadAlias(queryFilter)));
            } else {
                searchRequestBuilder.index(queryAlias);
            }
        }
        return searchRequestBuilder;
    }

}
