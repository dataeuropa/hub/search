package io.piveau.hub.search.verticles;

import io.piveau.hub.search.services.catalogues.CataloguesService;
import io.piveau.hub.search.services.datasets.DatasetsService;
import io.piveau.hub.search.services.search.SearchService;
import io.piveau.hub.search.services.sitemaps.SitemapsService;
import io.piveau.hub.search.services.vocabulary.VocabularyService;
import io.piveau.hub.search.Constants;
import io.piveau.json.ConfigHelper;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.shell.ShellService;
import io.vertx.ext.shell.ShellServiceOptions;
import io.vertx.ext.shell.command.Command;
import io.vertx.ext.shell.command.CommandBuilder;
import io.vertx.ext.shell.command.CommandProcess;
import io.vertx.ext.shell.command.CommandRegistry;
import io.vertx.ext.shell.term.HttpTermOptions;
import io.vertx.ext.shell.term.TelnetTermOptions;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Command line of piveau-hub-search
 * To see all available commands, execute <code>listCommands</code>
 */
public class ShellVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(ShellVerticle.class);

    private SearchService searchService;
    private DatasetsService datasetsService;
    private CataloguesService cataloguesService;
    private SitemapsService sitemapsService;
    private VocabularyService vocabularyService;

    @Override
    public void start(Promise<Void> startPromise) {
        searchService = SearchService.createProxy(vertx, SearchService.SERVICE_ADDRESS);
        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS);
        sitemapsService = SitemapsService.createProxy(vertx, SitemapsService.SERVICE_ADDRESS);
        vocabularyService = VocabularyService.createProxy(vertx, VocabularyService.SERVICE_ADDRESS);

        JsonObject cliConfig = ConfigHelper.forConfig(config())
                .forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_CLI_CONFIG);

        ShellServiceOptions shellServiceOptions = new ShellServiceOptions()
                .setWelcomeMessage("\nWelcome to piveau-hub-search CLI!\n\n");

        cliConfig.getMap().keySet().forEach(key -> {
            JsonObject options = cliConfig.getJsonObject(key);
            if (key.equals("http")) {
                shellServiceOptions.setHttpOptions(new HttpTermOptions()
                        .setHost(options.getString("host", "0.0.0.0"))
                        .setPort(options.getInteger("port", 8081))
                );
            }
            if (key.equals("telnet")) {
                shellServiceOptions.setTelnetOptions(new TelnetTermOptions()
                        .setHost(options.getString("host", "0.0.0.0"))
                        .setPort(options.getInteger("port", 5000))
                );
            }
        });

        ShellService shellService = ShellService.create(vertx, shellServiceOptions);

        shellService.start(handler -> {
            if (handler.succeeded()) {
                LOG.info("Successfully launched cli");
//                startPromise.complete();
            } else {
                LOG.error("Failed to launch cli", handler.cause());
//                startPromise.fail(handler.cause());
            }
        });

        CommandRegistry registry = CommandRegistry.getShared(vertx);
        List<Future<Command>> commandList = List.of(
                registry.registerCommand(buildSetMaxAggSizeCommand().build(vertx)),
                registry.registerCommand(buildSetMappingCommand().build(vertx)),
                registry.registerCommand(buildCreateIndexCommand().build(vertx)),
                registry.registerCommand(buildRemoveIndexCommand().build(vertx)),
                registry.registerCommand(buildSetReadAliasCommand().build(vertx)),
                registry.registerCommand(buildSetWriteAliasCommand().build(vertx)),
                registry.registerCommand(buildSetNumberOfReplicasCommand().build(vertx)),
                registry.registerCommand(buildResetIndicesCommand().build(vertx)),
                registry.registerCommand(buildSetMaxResultWindowCommand().build(vertx)),
                registry.registerCommand(buildBoostFieldCommand().build(vertx)),
                registry.registerCommand(buildReindexCataloguesCommand().build(vertx)),
                registry.registerCommand(buildSyncScoresCommand().build(vertx)),
                registry.registerCommand(buildIndexXmlVocabulariesCommand().build(vertx)),
                registry.registerCommand(buildResetWebrootCommand().build(vertx)),
                registry.registerCommand(buildTriggerSitemapGenerationCommand().build(vertx))
        );

        // List all commands
        CommandBuilder listCommands = CommandBuilder.command("listCommands");
        listCommands.processHandler(process -> {
            commandList.forEach(command ->
                    process.write(command.result().name() + "\n"));
            process.end();
        });
        registry.registerCommand(listCommands.build(vertx));

        Future.all(commandList)
                .onSuccess(c -> startPromise.complete())
                .onFailure(cause -> startPromise.fail(cause));
    }

    private CommandBuilder buildResetWebrootCommand() {
        CommandBuilder resetWebroot = CommandBuilder.command("resetWebroot");
        return resetWebroot.processHandler(process -> {
            if (vertx.fileSystem().existsBlocking("conf/webroot")) {
                vertx.fileSystem().deleteRecursiveBlocking("conf/webroot", true);
                process.write("Successfully reset webroot.\n");
            } else {
                process.write("Nothing to do here.\n");
            }
            process.end();
        });
    }

    private CommandBuilder buildSetMaxAggSizeCommand() {
        CommandBuilder setMaxAggSize = CommandBuilder.command("setMaxAggSize");
        return setMaxAggSize.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 2) {
                process.write("setMaxAggSize: try 'setMaxAggSize index number (>0)'\n");
                process.end();
            } else {
                try {
                    String index = args.get(0);
                    Integer maxAggSize = Integer.parseInt(args.get(1));
                    searchService.setMaxAggSize(index, maxAggSize).onComplete(ar -> handleResponse(process, ar));
                } catch (NumberFormatException e) {
                    process.write("setMaxAggSize: try 'setMaxAggSize index number (>0)'\n");
                    process.end();
                }
            }
        });
    }

    private CommandBuilder buildSetMappingCommand() {
        CommandBuilder setMapping = CommandBuilder.command("setMapping");
        return setMapping.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 1) {
                process.write("setMapping: try 'setMapping index'\n");
                process.end();
            } else {
                String index = args.get(0);
                searchService.putMapping(index).onComplete(ar -> handleResponse(process, ar));
            }
        });
    }

    private CommandBuilder buildCreateIndexCommand() {
        CommandBuilder createIndex = CommandBuilder.command("createIndex");
        return createIndex.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 1 && args.size() != 2) {
                process.write("createIndex: try 'createIndex index numberOfShards'\n");
                process.end();
            } else {
                try {
                    String index = args.get(0);
                    Integer numberOfShards = args.size() == 2 ? Integer.parseInt(args.get(1)) : null;
                    searchService.indexCreate(index, numberOfShards).onComplete(ar -> handleResponse(process, ar));
                } catch (NumberFormatException e) {
                    process.write("Value should be an integer\n");
                    process.end();
                }
            }
        });
    }

    private CommandBuilder buildRemoveIndexCommand() {
        CommandBuilder removeIndex = CommandBuilder.command("removeIndex");
        return removeIndex.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 1 && !(args.get(0).equals("vocabularies") && args.size() == 2)) {
                process.write("removeIndex: try 'removeIndex index'\n");
                process.end();
            } else {
                String index = args.get(0);
                searchService.indexDelete(index).onComplete(ar -> handleResponse(process, ar));
            }
        });
    }

    private CommandBuilder buildSetReadAliasCommand() {
        CommandBuilder setReadAlias = CommandBuilder.command("setReadAlias");
        return setReadAlias.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 1) {
                process.write("setReadAlias: try 'setReadAlias index'\n");
                process.end();
            }
            String index = args.get(0);
            String prefix = index.substring(0, index.lastIndexOf("_"));
            searchService.setIndexAlias("*", index, prefix + "_read").onComplete(ar -> handleResponse(process, ar));
        });
    }

    private CommandBuilder buildSetWriteAliasCommand() {
        CommandBuilder setWriteAlias = CommandBuilder.command("setWriteAlias");
        return setWriteAlias.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 1) {
                process.write("setWriteAlias: try 'setWriteAlias index'\n");
                process.end();
            }
            String index = args.get(0);
            String prefix = index.substring(0, index.lastIndexOf("_"));
            searchService.setIndexAlias("*", index, prefix + "_write").onComplete(ar -> handleResponse(process, ar));
        });
    }

    private CommandBuilder buildSetNumberOfReplicasCommand() {
        CommandBuilder setNumberOfReplicas = CommandBuilder.command("setNumberOfReplicas");
        return setNumberOfReplicas.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 2) {
                process.write("setNumberOfReplicas: try 'setNumberOfReplicas index number'\n");
                process.end();
            } else {
                String index = args.get(0);
                Integer numberOfReplicas = Integer.parseInt(args.get(1));
                searchService.setNumberOfReplicas(index, numberOfReplicas).onComplete(
                        ar -> handleResponse(process, ar));
            }
        });
    }

    private CommandBuilder buildResetIndicesCommand() {
        CommandBuilder resetIndices = CommandBuilder.command("resetIndices");
        return resetIndices.processHandler(process -> {
            process.write("Are you sure you want to reset all indexes? [y/n]\n");
            process.interruptHandler(v -> process.end());
            process.stdinHandler(data -> {
                if (data.equals("y") || data.equals("Y")) {
                    searchService.indexReset().onComplete(indexResetHandler -> {
                        if (indexResetHandler.succeeded()) {
                            process.write("Successfully reset index\n");
                        } else {
                            process.write(indexResetHandler.cause().toString());
                        }
                        process.end();
                    });
                } else if (data.equals("n") || data.equals("N")) {
                    process.end();
                }
            });
        });
    }

    private CommandBuilder buildSetMaxResultWindowCommand() {
        CommandBuilder setMaxResultWindow = CommandBuilder.command("setMaxResultWindow");
        return setMaxResultWindow.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 2) {
                process.write("setMaxResultWindow: try 'setMaxResultWindow index value'\n");
                process.end();
            } else {
                try {
                    String index = args.get(0);
                    Integer maxResultWindow = Integer.parseInt(args.get(1));
                    searchService.setMaxResultWindow(index, maxResultWindow).onComplete(
                            ar -> handleResponse(process, ar));
                } catch (NumberFormatException e) {
                    process.write("Value should be an integer\n");
                    process.end();
                }
            }
        });
    }

    private CommandBuilder buildBoostFieldCommand() {
        CommandBuilder boostField = CommandBuilder.command("boostField");
        return boostField.processHandler(process -> {
            List<String> args = process.args();
            if (args.size() != 3) {
                process.write("boostField: try 'boostField filter field value'\n");
                process.end();
            } else {
                try {
                    String indexPrefix = args.get(0);
                    String field = args.get(1);
                    Float value = Float.parseFloat(args.get(2));
                    searchService.boost(indexPrefix, field, value).onComplete(ar -> handleResponse(process, ar));
                } catch (NumberFormatException e) {
                    process.write("Value should be a float\n");
                    process.end();
                }
            }
        });
    }

    private CommandBuilder buildReindexCataloguesCommand() {
        CommandBuilder reindexCatalogues = CommandBuilder.command("reindexCatalogues");
        return reindexCatalogues.processHandler(process -> {
            process.write("Are you sure you want to reindex all catalogues? [y/n]\n");
            process.interruptHandler(v -> process.end());
            process.stdinHandler(data -> {
                if ("y".equalsIgnoreCase(data)) {
                    JsonObject query = createSearchQuery();
                    searchService.search(query.toString()).onComplete(searchResult -> {
                        if (searchResult.succeeded()) {
                            JsonObject result = searchResult.result().getJsonObject("result");
                            JsonArray results = result.getJsonArray("results");

                            reindexCatalogs(results, process);
                        } else {
                            process.write(searchResult.cause().getMessage() + "\n");
                            process.end();
                        }
                    });
                } else if ("n".equalsIgnoreCase(data)) {
                    process.end();
                }
            });
        });
    }

    private CommandBuilder buildSyncScoresCommand() {
        CommandBuilder syncScores = CommandBuilder.command("syncScores");
        return syncScores.processHandler(process -> datasetsService.triggerSyncScores()
                .onSuccess(result -> {
                    process.write(result + "\n");
                    process.end();
                }).onFailure(failure -> {
                    process.write("SyncScore failure: " + failure + "\n");
                    process.end();
                })
        );
    }

    private CommandBuilder buildIndexXmlVocabulariesCommand() {
        CommandBuilder indexXmlVocabularies = CommandBuilder.command("indexXmlVocabularies");
        WebClient webClient = WebClient.create(vertx);
        return indexXmlVocabularies.processHandler(process -> {
            List<Future<Void>> futureList = new ArrayList<>();
            Future<Void> ianaMediaTypesFuture = indexXmlVocabulary(webClient, "iana-media-types",
                    "https://www.iana.org/assignments/media-types/media-types.xml",
                    "conf/vocabularies/iana-media-types.xslt");
            futureList.add(ianaMediaTypesFuture);
            Future<Void> spdxChecksumAlgorithmFuture = indexXmlVocabulary(webClient, "spdx-checksum-algorithm",
                    "https://raw.githubusercontent.com/spdx/spdx-spec/support/v2.3/ontology/spdx-ontology.owl.xml",
                    "conf/vocabularies/spdx-checksum-algorithm.xslt");
            futureList.add(spdxChecksumAlgorithmFuture);
            Future.all(futureList).onSuccess(result -> {
                process.write("Successfully indexed xml vocabularies\n");
                process.end();
            }).onFailure(failure -> {
                process.write(failure.getMessage() + "\n");
                process.end();
            });
        });
    }

    private CommandBuilder buildTriggerSitemapGenerationCommand() {
        CommandBuilder triggerSitemapGeneration = CommandBuilder.command("triggerSitemapGeneration");
        return triggerSitemapGeneration.processHandler(process -> sitemapsService.triggerSitemapGeneration().onComplete(ar -> {
            if (ar.succeeded()) {
                process.write(ar.result().getString("result") + "\n");
                process.end();
            }
        }));
    }

    private Future<Void> indexXmlVocabulary(WebClient webClient, String id, String url, String xsltPath) {
        Promise<Void> promise = Promise.promise();
        webClient.getAbs(url).send()
                .onSuccess(result -> {
                    try {
                        // Use net.sf.saxon transformer for XSLT 2.0
                        TransformerFactory factory = TransformerFactory.newInstance();
                        Source xslt = new StreamSource(new File(xsltPath));
                        Transformer transformer = factory.newTransformer(xslt);

                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        transformer.transform(new StreamSource(new StringReader(result.bodyAsString())), new StreamResult(out));

                        String output = out.toString(StandardCharsets.UTF_8);

                        vocabularyService.createOrUpdateVocabulary(id, new JsonObject(output)).onComplete(ar -> {
                            if (ar.succeeded()) {
                                promise.complete();
                            } else {
                                promise.fail(ar.cause());
                            }
                        });

                    } catch (TransformerException e) {
                        promise.fail(id + " transformation error: " + e.getMessage());
                    } catch (Exception e) {
                        promise.fail("An error occurred: " + e.getMessage());
                    }
                }).onFailure(promise::fail);
        return promise.future();
    }

    private JsonObject createSearchQuery() {
        List<String> filters = List.of("catalogue");

        return new JsonObject()
                .put("filters", filters)
                .put("from", 0)
                .put("size", 1000)
                .put("elasticId", true);
    }

    private void reindexCatalogs(JsonArray results, CommandProcess process) {
        List<Future<Void>> futureList = new ArrayList<>();

        results.forEach(value -> {
            Promise<Void> valuePromise = Promise.promise();
            ((JsonObject) value).remove("count");
            String id = ((JsonObject) value).remove("_id").toString();
            final JsonObject valueJson = ((JsonObject) value);
            cataloguesService.createOrUpdateCatalogue(id, valueJson).onComplete(replaceResult -> {
                if (replaceResult.succeeded()) {
                    process.write(valueJson.getString("id") + "\n");
                    valuePromise.complete();
                } else {
                    process.write(replaceResult.cause().getMessage() + "\n");
                    valuePromise.complete();
                }
            });
            futureList.add(valuePromise.future());
        });

        Future.all(futureList).onComplete(ar -> process.end());
    }

    private void handleResponse(CommandProcess process, AsyncResult<String> ar) {
        if (ar.succeeded()) {
            process.write(ar.result() + "\n");
        } else {
            process.write(ar.cause().getMessage() + "\n");
        }
        process.end();
    }

}
