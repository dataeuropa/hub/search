/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search.security;

import io.vertx.codegen.annotations.*;
import io.vertx.core.*;
import io.vertx.core.json.*;
import org.apache.commons.lang3.*;

import java.util.*;

@DataObject
public class AccessControlContext {
    private String userId;
    private JsonArray permissions;
    private List<String> datasetIds = new ArrayList<>();
    private List<String> catalogIds = new ArrayList<>();

    private static final String PUBLIC_FLAG = "publicFlag";
    private static final String DATASETS = "datasetIds";
    private static final String CATALOGS = "catalogIds";

    public AccessControlContext(JsonObject json) {
        userId = json.getString("userId");
        permissions = json.getJsonArray("permissions");
    }
    public AccessControlContext(String userId, JsonArray permissions) {
        this.userId = userId;
        this.permissions = permissions;
        parsePermissions();
    }
    public AccessControlContext() {}

    private void parsePermissions() {
        permissions.stream()
                .filter(JsonObject.class::isInstance)
                .map(JsonObject.class::cast)
                .filter(permission -> (permission.containsKey("scopes") && permission.getJsonArray("scopes").contains("dataset:view")))
                .forEach(permission -> datasetIds.add(StringUtils.substringAfterLast(permission.getString("rsname"), "/")));
        permissions.stream()
                .filter(JsonObject.class::isInstance)
                .map(JsonObject.class::cast)
                .filter(permission -> (permission.containsKey("rsname") && permission.getString("rsname").chars().filter(ch -> ch == '/').count() == 1))
                .filter(permission -> (permission.containsKey("scopes") && permission.getJsonArray("scopes").contains("dataset:view_published")))
                .forEach(permission -> catalogIds.add(StringUtils.substringAfterLast(permission.getString("rsname"), "/")));
    }

    public boolean userPresent() {
        return userId != null;
    }

    public JsonArray getPermissions() {
        return permissions;
    }

    public List<String> getDatasetIds() {
        return datasetIds;
    }
    public JsonObject toJson() {
        return new JsonObject().put("userId", userId).put("permissions", permissions);
    }

    public Boolean datasetAccess(JsonObject dataset) {
        JsonObject payload = dataset.getJsonObject("result");
        if (!payload.containsKey("public") || payload.getValue("public") == null || payload.getBoolean("public")) {
            return true;
        } else {
            String catalogId = payload.getJsonObject("catalog").getString("id");
            if (catalogIds.contains(catalogId)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void mergeWithQuery(JsonObject query) {
        query.put(PUBLIC_FLAG, "true");
        query.put(DATASETS, new JsonArray(datasetIds));
        query.put(CATALOGS, new JsonArray(catalogIds));
    }

}
