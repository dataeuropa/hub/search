/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search.security;

import io.piveau.hub.search.*;
import io.vertx.core.json.*;

import java.util.*;

public class AccessControlUtils {

    public final static String featureFlag = "access_control";

    public static Boolean isEnabled() {
        return FeatureFlags.isEnabled(AccessControlUtils.featureFlag);
    }

    public static void setPublicInDataset(JsonObject dataset, JsonObject catalog) {
        if (AccessControlUtils.isEnabled()) {
            dataset.put("public", catalog.getBoolean("public"));
        }
    }

    public static void setPublicReplacement(List<String> replacements) {
        if (AccessControlUtils.isEnabled()) {
            replacements.add("public");
        }
    }
}
