package io.piveau.hub.search;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Collection;
import java.util.Map;

public class Helper {
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isNullOrEmpty(Map<?,?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNullOrEmpty(JsonArray jsonArr) {
        return jsonArr == null || jsonArr.isEmpty();
    }

    public static boolean isNullOrEmpty(JsonObject jsonObj) {
        return jsonObj == null || jsonObj.isEmpty();
    }
}
